<?php
$obj = request()->route();
if($obj) {
    $route = $obj->getName();
} else {
    $route = "404";
}

$user_desc = config('app.short_bangla').' আমার অনুপ্রেরণার উৎস।';
$general_desc = config('app.short_bangla').' - একটি জ্ঞানের আলোকবর্তিকা। যেখানে স্বপ্ন হাতছানি দেয়, মায়াবি সবুজ ক্যাম্পাসের প্রতিটি বৃক্ষ রূপকথার গল্প শোনায়। (Hasanpur College)';
if($route == "blog.show") {
    $general_desc = excerpt($post->body, 100);
}
?>
<meta charset="utf-8">
<meta name="keywords" content="<?php if(isset($user)): ?> <?php echo e(url('/images/profile/$user->photo')); ?> <?php else: ?> <?php if(isset($title)): ?> <?php echo e($title); ?> <?php else: ?> <?php echo $__env->yieldContent('title'); ?> <?php endif; ?>, <?php echo e(config('app.short_bangla')); ?> <?php endif; ?>">
<meta name="description" content="<?php if(isset($user)): ?> <?php echo e($user->note ?? $user_desc); ?> <?php else: ?> <?php echo e($general_desc); ?> <?php endif; ?>" />
<meta name="author" content="ict4today">
<meta name="theme-color" content="<?php echo e(env('theme_color') ?? '#0057a8'); ?>" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<meta name="client-secret" content="<?php echo e(config('app.hackerearth_client_secret')); ?>">

<meta property="og:url"                content="<?php echo e(url()->current()); ?>" />
<meta property="og:type"               content="<?php if(isset($user)): ?> profile <?php else: ?> article <?php endif; ?>" />
<meta property="og:title"              content="<?php if(isset($title)): ?> <?php echo e($title); ?> <?php else: ?> <?php echo $__env->yieldContent('title'); ?> <?php endif; ?>" />
<meta property="og:description"        content="<?php if(isset($user)): ?> <?php echo e($user->note ?? $user_desc); ?> <?php else: ?> <?php echo e($general_desc); ?> <?php endif; ?>" />
<meta property="og:image"              content="<?php if(isset($user)): ?> <?php echo e(url('/images/profile/$user->photo')); ?> <?php else: ?> <?php echo e(url('/images/logo.png')); ?> <?php endif; ?>" />

<title><?php if(isset($title)): ?> <?php echo e($title); ?> <?php else: ?> <?php echo $__env->yieldContent('title'); ?> <?php endif; ?></title>

<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">

<?php
    if($route == "users.show") {
        $url = url("users/".$user->id.'/'.urlencode($user->name));
    } elseif($route == "attendances.show") {
        $url = url("attendances/".$user->hsc."/".$user->roll.'/'.urlencode($user->name));
    } elseif($route == "notices.show") {
        $url = url("notices/".$post->id.'/'.urlencode($post->title));
    } elseif($route == "blog.show") {
        $url = url("blog/".$post->id.'/'.urlencode($post->title));
    } else {
        $url = url()->current();
    }
?>
<link rel="canonical" href="<?php echo e($url ?? ''); ?>">
<?php echo $__env->make('layouts.schema', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Scripts -->
<?php echo app('Illuminate\Foundation\Vite')('resources/css/app.scss'); ?>
<?php echo $__env->yieldContent('style'); ?><?php /**PATH C:\xampp\htdocs\ai\resources\views/layouts/meta.blade.php ENDPATH**/ ?>