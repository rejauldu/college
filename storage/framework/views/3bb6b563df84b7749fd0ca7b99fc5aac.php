<?php $__env->startSection('title'); ?>
<?php echo e($post->title ?? ''); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container mx-auto min-h-screen">
    <div class="grid lg:grid-cols-[auto_1fr_auto] py-3">
        <div class="hidden lg:block">
            <?php echo $__env->make('layouts.info', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <div class="lg:px-3">
            <h1 class="text-3xl capitalize"><?php echo e($post->title ?? ''); ?></h1>
            <div class="mb-3">
                <p class="text-gray-500 text-sm">Posted at: <?php echo e($post->created_at->format('jS M Y')); ?></p>
            </div>
            <div class="mb-3">
                <?php echo e($post->body ?? ''); ?>

            </div>
            <?php
            $pathtofile = '/posts/'.$post->link;
            $info = pathinfo($pathtofile);
            ?>
            <?php if(isset($info["extension"]) && $info["extension"] == "pdf"): ?>
                <object data="/posts/<?php echo e($post->link); ?>" type="application/pdf" width="600" height="500" class="w-full overflow-hidden border-4">
                    <p class="bg-theme-light p-3 rounded"><a class="underline" href="/posts/<?php echo e($post->link); ?>" download><?php echo e($post->title ?? 'Download'); ?></a></p>
                    <embed src="/posts/<?php echo e($post->link); ?>" width="100%" height="600px"/>
                </object>
            <?php else: ?>
                <img class="w-full" src="/posts/<?php echo e($post->link); ?>" alt="<?php echo e($post->title ?? ''); ?>" />
            <?php endif; ?>
        </div>
        <div class="hidden lg:block">
            <?php echo $__env->make('layouts.subject', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ai\resources\views/blog/show.blade.php ENDPATH**/ ?>