<div class="bg-theme-primary py-1">
    <div class="container mx-auto">
        <div class="flex justify-between text-white text-sm">
            <div class="flex gap-3">
                <a href="callto:<?php echo e(config('app.phone')); ?>" class="flex gap-1 group hover:text-black transition-all duration-300"><?php echo e(getSVG('/images/icons/phone.svg')); ?> <?php echo e(config('app.phone')); ?></a>
                <a href="mailto:<?php echo e(config('app.email')); ?>" class="gap-1 group hover:text-black transition-all duration-300 hidden md:flex"><?php echo e(getSVG('/images/icons/envelope.svg')); ?> <?php echo e(config('app.email')); ?></a>
            </div>
            <a href="<?php echo e(config('app.fb')); ?>" class="flex gap-1 text-white hover:text-black" aria-label="Join our Facebook group"><?php echo e(getSVG('/images/icons/facebook.svg')); ?></a>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-sm !bg-gradient-to-b from-[#acf] via-gray-[#eeefff] to-white" id="navbar">
    <div class="container mx-auto">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#header-menu" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="i4t-collapse navbar-collapse">
            <ul id="main-menu" class="navbar-nav">
                <li class="menu-item <?php if(Route::currentRouteName() == 'app.home'): ?> current-menu-item <?php endif; ?>"><a href="/">Home</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'routines.index'): ?> current-menu-item <?php endif; ?>"><a class="text-white" href="<?php echo e(route('routines.index')); ?>">Routine</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'attendances.index'): ?> current-menu-item <?php endif; ?>"><a class="text-white" href="<?php echo e(route('attendances.index')); ?>">Attendance</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'results.index'): ?> current-menu-item <?php endif; ?>"><a class="text-white" href="<?php echo e(route('results.index')); ?>">Result</a></li>
                <li id="menu-item" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item"><a href="<?php echo e(route('users.teachers')); ?>">Members</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item <?php if(Route::currentRouteName() == 'users.teachers'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('users.teachers')); ?>">Faculty Members</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item <?php if(Route::currentRouteName() == 'users.staffs'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('users.staffs')); ?>">Our Staff</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item <?php if(Route::currentRouteName() == 'users.students'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('users.students')); ?>">Students</a></li>
                    </ul>
                </li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'app.c'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('app.c')); ?>">C Compiler</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'app.about'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('app.about')); ?>">About</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'app.contact'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('app.contact')); ?>">Contact</a></li>
                <li class="menu-item <?php if(Route::currentRouteName() == 'app.wall'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('app.wall')); ?>">Wall</a></li>
                <?php if(auth()->guard()->guest()): ?>
                <li class="menu-item <?php if(Route::currentRouteName() == 'login'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('login')); ?>">Login</a></li>
                <?php else: ?>
                <?php
                $user = auth()->user()
                ?>
                <li class="menu-item <?php if(Route::currentRouteName() == 'users.show'): ?> current-menu-item <?php endif; ?>"><a href="<?php echo e(route('users.show', $user->id)); ?>/<?php echo e($user->name); ?>"><?php echo e(fname($user->name)); ?></a></li>
                
                <?php endif; ?>
            </ul>
		</div>
	</div>
</nav><?php /**PATH C:\xampp\htdocs\ai\resources\views/layouts/header.blade.php ENDPATH**/ ?>