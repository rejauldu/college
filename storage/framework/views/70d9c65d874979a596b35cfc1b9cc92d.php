<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8895896076224126" crossorigin="anonymous"></script>
<?php if(isset($script)): ?>
    <?php echo app('Illuminate\Foundation\Vite')("resources/js/$script/main.js"); ?>
<?php else: ?>
    <?php echo app('Illuminate\Foundation\Vite')("resources/js/app.js"); ?>
<?php endif; ?>
<script>
    window.Laravel = {
        isLoggedIn: <?php echo e(Auth::check() ? 'true' : 'false'); ?>,
        user: <?php echo json_encode(Auth::user(), 15, 512) ?>
    };
</script><?php /**PATH C:\xampp\htdocs\ai\resources\views/layouts/scripts.blade.php ENDPATH**/ ?>