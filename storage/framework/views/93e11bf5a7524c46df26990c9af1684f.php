<?php $__env->startSection('title'); ?>
Blog Post | ব্লগ পোস্ট
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container mx-auto g">
    <div class="grid lg:grid-cols-[auto_1fr_auto]">
        <div class="hidden lg:block"></div>
        <div class="lg:px-3 grid gap-4">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="grid gap-2 grid-cols-[auto_1fr]">
                <div class=""><img src="<?php echo e(url('/')); ?>/image/80/80/<?php echo e($post->link ?? 'default.webp'); ?>" alt="<?php echo e($post->title ?? ''); ?>" class="w-20 h-20" /></div>
                <div class="">
                    <h4><a href="<?php echo e(route('blog.show', $post->id)); ?>/<?php echo e(urlencode($post->title)); ?>"><?php echo e($post->title); ?></a></h4>
                    <hr/>
                    <span class="text-sm text-gray-400">Posted at <?php echo e($post->created_at->format('jS M Y')); ?></span>
                    <hr/>
                    <div class="text-justify"><?php echo excerpt($post->body, 30); ?> <a class="btn btn-link text-theme-primary" href="<?php echo e(route('blog.show', $post->id)); ?>/<?php echo e(urlencode($post->title)); ?>">...more</a></div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="hidden lg:block"></div>
    </div>
    <div>
        <?php echo e($posts->links()); ?>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ai\resources\views/blog/index.blade.php ENDPATH**/ ?>