<h3 class="p-3 bg-theme-primary text-white text-center">কলেজ সম্পর্কিত তথ্য</h3>
<ul class="list w-80">
    <li><a href="<?php echo e(route('users.teachers')); ?>">Faculty members</a></li>
    <li><a href="<?php echo e(route('users.staffs')); ?>">Office staffs</a></li>
    <li><a href="<?php echo e(route('users.students')); ?>">Student info</a></li>
    <li><a href="<?php echo e(route('routines.index')); ?>">Class routine</a></li>
    <li><a href="<?php echo e(route('blog.index')); ?>">Blog</a></li>
    <li><a href="<?php echo e(route('academic-rules')); ?>">Academic Rules</a></li>
    <li><a href="<?php echo e(url('assets/সরকারি-ছুটির-তালিকা.webp')); ?>">Holiday Calendar</a></li>
    <li><a href="https://dhakaeducationboard.gov.bd/index.php/site/product/hsccorner">Syllabus</a></li>
    <li><a href="http://nctb.gov.bd/site/page/ac3e2b79-5045-4b87-9a5e-f5b59a53e0ee">Book list</a></li>
</ul><?php /**PATH C:\xampp\htdocs\ai\resources\views/layouts/info.blade.php ENDPATH**/ ?>