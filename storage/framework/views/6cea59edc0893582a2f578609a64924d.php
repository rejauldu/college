<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "<?php echo e(config('app.name')); ?>",
  "url": "<?php echo e(config('app.url')); ?>",
  "logo": "<?php echo e(config('app.url')); ?>/images/logo.png",
  "sameAs": [
    "<?php echo e(config('app.fb')); ?>"
  ],
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "<?php echo e(config('app.phone')); ?>",
    "contactType": "Admissions Office",
    "areaServed": "Bangladesh",
    "availableLanguage": ["English", "Bengali"]
  },
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "<?php echo e(config('app.address')); ?>",
    "addressLocality": "Cumilla",
    "addressRegion": "Chattogram",
    "postalCode": "3516",
    "addressCountry": "Bangladesh"
  },
  "foundingDate": "1969",
  "description": "<?php echo e(config('app.short_bangla')); ?> - যেখানে স্বপ্ন হাতছানি দেয়, ঝরে পড়া ফুল আবার নতুন জীবন খুঁজে পায়। যেখানে মায়াবি সবুজ ক্যাম্পাসের প্রতিটি বৃক্ষ রূপকথার গল্প শোনায়।"
}
</script><?php /**PATH C:\xampp\htdocs\ai\resources\views/layouts/schema.blade.php ENDPATH**/ ?>