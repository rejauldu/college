<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <?php echo $__env->make('layouts.meta', ['title' => "Page not found"], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </head>
    <body class="font-sans antialiased">
        <div class="grid grid-rows-[auto_auto_1fr_auto] min-h-screen">
            <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="container mx-auto text-center py-5">
                <h1 class="text-red-500">404</h1>
                <p class="text-xl">Oops! The page you're looking for can't be found.</p>
                <p>It might have been removed, renamed, or didn't exist in the first place.</p>
                <a href="/" class="underline text-theme-primary">Go Back to Homepage</a>
            </div>
            <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->yieldContent('script'); ?>
        </div>
    </body>
</html><?php /**PATH C:\xampp\htdocs\ai\resources\views/errors/404.blade.php ENDPATH**/ ?>