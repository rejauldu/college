@extends('layouts.backend')
@section('title')
{{ __(isset($metas)?'Update meta':'Create meta') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($metas)?'Update meta':'Create meta') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                               <form action="{{ route('results.download') }}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="batch" class="form-label">Batch:</label>
                                        <select class="form-control" name="batch" id="batch" required>
                                            <option value="">--Select Batch--</option>
                                            <option value="{{ $batches->batch1 }}">{{ $batches->batch1 }}</option>
                                            <option value="{{ $batches->batch2 }}">{{ $batches->batch2 }}</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="type" class="form-label">Group:</label>
                                        <select class="form-control" name="group_id" id="type" required>
                                            <option value="">--Select Group--</option>
                                            @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="type" class="form-label">Exam Type:</label>
                                        <select class="form-control" name="exam_type_id" id="type" required>
                                            <option value="">--Select Exam Type--</option>
                                            @foreach($exam_types as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Download <i class="fa fa-download"></i></button>
                                </form>
                            </div><!--/col-9-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection