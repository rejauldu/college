@extends('layouts.dashboard')
@section('title')
{{ __(isset($metas)?'Update meta':'Create meta') }}
@endsection
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h3>SMS <small>{{ isset($metas)?'edit':'create' }}</small></h3>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">SMS</a></li>
                <li class="active">Send</li>
            </ol>
        </section>
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-edit"></i> Send</h3>
                        <div class="box-tools float-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row pt-2">
                            <div class="col-12">
                                <table class="table table-striped display-7">
                                    <tr>
                                        <th></th>
                                        <th>Arts</th>
                                        <th>Science</th>
                                        <th>Commerce</th>
                                    </tr>
                                    <tr>
                                        <th>1st year</th>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch2_arts_status" type="checkbox" {{ $status->batch2_arts_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch2_science_status" type="checkbox" {{ $status->batch2_science_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch2_commerce_status" type="checkbox" {{ $status->batch2_commerce_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>2nd year</th>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch1_arts_status" type="checkbox" {{ $status->batch1_arts_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch1_science_status" type="checkbox" {{ $status->batch1_science_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                        <td>
                                            <div class="form-check form-switch">
												<input class="form-check-input" name="batch1_commerce_status" type="checkbox" {{ $status->batch1_commerce_status == 1?'checked': 0 }} onchange="handleChange(event)">
											</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-12">
                                <div class="alert alert-info display-1 text-center" id="counter">{{ $remaining ?? 0 }}</div>
                            </div>
                            <div class="col-12">
                                <div class="progress mb-3 d-none">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" style="width:0%">
                                    40%
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12"><!--left col-->
                               <a href="#" class="btn btn-danger" onclick="sms();load()"><span class="spinner-border d-none" id="spinner"></span> Send SMS <i class="fa fa-arrow-right"></i></a>
                            </div><!--/col-9-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    function sms() {
        const total = {{ $remaining ?? 0 }};
        let remaining = 0;
        $.ajax({
            url: "/api/sms/send",
            type: 'GET',
            dataType: 'json',
            success: function(res) {
                remaining = res.remaining;
                document.getElementById("counter").innerText = remaining;
                document.querySelector(".progress-bar").innerText = (total-remaining)/total*100+'%';
                document.querySelector(".progress-bar").style.width = (total-remaining)/total*100+'%';
                if(remaining>0) {
                    sms();
                } else {
                    document.getElementById("spinner").classList.add("d-none");
                    document.querySelector(".progress").classList.add("d-none");
                }
            },
            error: function(e) {
                document.getElementById("counter").innerText = "Error";
            }
        });
    }
    function load() {
        document.getElementById("spinner").classList.remove("d-none");
        document.querySelector(".progress").classList.remove("d-none");
    }
    function handleChange(e) {
        const {checked, name} = e.target;
        $.ajax({
            url: "/api/app-meta",
            type: "post",
            data: {
                meta_key: name,
                meta_value: checked?1:0,
                _token: "{{ csrf_token() }}"
            }
        });
    }
    
</script>
@endsection