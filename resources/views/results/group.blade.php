@extends('layouts.dashboard')
@section('title')
{{ __(isset($attendance)?'Update meta':'Create meta') }}
@endsection
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h3>Attendance <small>{{ isset($attendance)?'edit':'create' }}</small></h3>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Blogs</a></li>
                <li class="active">{{ isset($attendance)?'Edit':'Create' }}</li>
            </ol>
        </section>
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-edit"></i> {{ __(isset($attendance)?'Update':'Create') }}</h3>
                        <div class="box-tools float-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                               <form action="{{ route('group.store') }}" method="post">
                                    @csrf
                                    <table class="table table-striped display-7">
                                        <tr>
                                            <th></th>
                                            <th>Arts</th>
                                            <th>Science</th>
                                            <th>Commerce</th>
                                        </tr>
                                        <tr>
                                            <th>1st year</th>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch2_arts" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch2_science" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch2_commerce" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>2nd year</th>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch1_arts" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch1_science" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" name="batch1_commerce" type="checkbox" checked value="1">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <button type="submit" class="btn btn-theme" {{ isset($dataToday) ? 'disabled':'' }}>Submit</button>
                                </form>
                            </div><!--/col-9-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('style')
<link href="https://code.jquery.com/ui/1.13.0/themes/base/jquery-ui.min.css" rel="stylesheet">
@endsection
@section('script')
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker();
    } );
</script>
@endsection