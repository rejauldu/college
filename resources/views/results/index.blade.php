@extends('layouts.app')
@section('title')
Result Card
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid items-center h-full">
        <form class="sm:p-5 border rounded w-full max-w-xxl mx-auto" onSubmit="return onSubmit(event)">
            @csrf
            <div class="mb-3">
                <label for="exam-type-id">Exam:</label>
                <select name="exam_type_id" id="exam-type-id" required>
                    <option value="">--Select Exam--</option>
                    @foreach($exam_types as $type)
                    <option value="{{ $type->id ?? '' }}">{{ $type->name ?? '' }}</option>
                    @endforeach
                </select>
            </div>
            <div class="flex gap-2">
                <input type="submit" id="submit-btn" class="button-primary mb-3" value="View Result" />
                <svg id="loading" class="hidden w-8 h-8 mx-auto text-theme-primary animate-spin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor">
                    <circle cx="12" cy="12" r="10" stroke-width="4" stroke="currentColor" fill="none" stroke-dasharray="60" stroke-dashoffset="40"></circle>
                    <path stroke-linecap="round" stroke-linejoin="round" d="M4 12a8 8 0 1 1 8 8 8 8 0 0 1-8-8z"></path>
                </svg>
            </div>
            <div id="result" class="text-center overflow-x-auto max-w-[calc(100vw_-_2rem)]"></div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script>
    function onSubmit(e) {
        e.preventDefault();
        var form = e.target;
        var submitButton = document.getElementById('submit-btn');
        var loadingImage = document.getElementById('loading');
        // Disable the submit button and show loading
        submitButton.disabled = true;
        submitButton.value = "Processing...";
        loadingImage.classList.remove('hidden'); // Show loading image

        // Access form data using FormData
        var formData = new FormData(form);

        fetch('api/results', {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .then(data => {
            if(data.status == 200)
                createResultTable(data);
            else
                notFound();
        })
        .finally(() => {
            // Re-enable the submit button and hide the loading image
            submitButton.disabled = false;
            submitButton.value = "View Result";
            loadingImage.classList.add('hidden');
        });
        return false
    }
    
    function createResultTable(data) {
        const result = data.result, user = data.user, id=data.type_id;
        let total = 0, gp = 0, point = 0, grade, passStatus;
        let totalPoints = 0;
        let totalSubjects = 0;
        let allSubjectsPassed = true; // Assume all subjects passed initially
        var tableContainer = document.getElementById('result');

        // Clear existing content in the tableContainer
        tableContainer.innerHTML = '';

        // Access student information from user object
        var studentName = user.name;
        var studentRoll = user.roll;
        var studentPhone = user.phone;

        // Append title to the container
        var heading = document.createElement('h2');
        heading.innerHTML = "Result Card";
        heading.classList.add('print-only');
        heading.classList.add('text-center');
        heading.classList.add('mt-10');
        heading.classList.add('text-theme-primary');
        tableContainer.appendChild(heading);

        // Append college name to the container
        var heading = document.createElement('h3');
        heading.innerHTML = "Hasanpur Shahid Nazrul Government College";
        heading.classList.add('print-only');
        heading.classList.add('text-center');
        heading.classList.add('capitalize');
        tableContainer.appendChild(heading);

        // Append college name to the container
        var heading = document.createElement('p');
        heading.innerHTML = "Exam: "+data.type;
        heading.classList.add('print-only');
        heading.classList.add('text-center');
        heading.classList.add('mb-5');
        tableContainer.appendChild(heading);

        // Display print button
        var printButton = document.createElement('button');
        printButton.innerHTML = `<span class="mr-2">Print</span> {{ getSVG("/images/icons/print.svg") }}`;
        printButton.setAttribute("onclick", "printDiv('result')");
        printButton.classList.add('button-primary');
        printButton.classList.add('!bg-red-500');
        printButton.classList.add('ml-auto');
        tableContainer.appendChild(printButton);

        // Display student information
        var infoTable = document.createElement('table');
        infoTable.classList.add('table');
        infoTable.classList.add('mb-4');
        infoTable.classList.add('!text-left');

        var infoRow = document.createElement('tr');
        infoRow.classList.add('!bg-white');
        var nameCell = document.createElement('td');
        nameCell.textContent = 'Name: ' + studentName;
        infoRow.appendChild(nameCell);

        var rollCell = document.createElement('td');
        rollCell.textContent = 'Roll: ' + studentRoll;
        infoRow.appendChild(rollCell);

        var phoneCell = document.createElement('td');
        phoneCell.textContent = 'Phone: ' + studentPhone;
        infoRow.appendChild(phoneCell);

        var passStatusCell = document.createElement('td');
        passStatusCell.textContent = 'Edu. Year: ' + (user.hsc - 2) + ' - ' + (user.hsc - 1);
        infoRow.appendChild(passStatusCell);

        infoTable.appendChild(infoRow);
        tableContainer.appendChild(infoTable);

        var markSheet = document.createElement('h4');
        markSheet.textContent = 'Mark Sheet';
        markSheet.classList.add('capitalize');
        markSheet.classList.add('text-center');
        tableContainer.appendChild(markSheet);

        // Create a table element for the result
        var table = document.createElement('table');
        table.classList.add('table');
        table.classList.add('table-stripe');

        // Create headers row
        var headersRow = document.createElement('tr');

        // Create header cells
        let headers = ['Subject', 'CQ', 'MCQ', 'Practical', 'Total', 'Grade Point', 'Grade'];
        if(id>=10)
            headers = ['Subject', 'CQ', 'MCQ', 'Total', 'Grade Point', 'Grade'];
        headers.forEach(headerText => {
            var headerCell = document.createElement('th');
            headerCell.textContent = headerText;
            headersRow.appendChild(headerCell);
        });
        // Append the headers row to the table
        table.appendChild(headersRow);

        // Populate the table with rows for each field
        for (var key in result) {
            if (result[key] !== null && result[key].cq !== null) {
                total = 0;

                // Create a table row
                var row = document.createElement('tr');

                // Create a cell for the label
                var labelCell = document.createElement('td');
                labelCell.textContent = key;
                row.appendChild(labelCell);

                // Create cells for the values
                let cells = ['cq', 'mcq', 'prac'];
                if(id>=10)
                    cells = ['cq', 'mcq'];
                cells.forEach(type => {
                    var valueCell = document.createElement('td');
                    let mark = result[key][type];
                    valueCell.textContent = mark<0?'Absent':mark;
                    row.appendChild(valueCell);
                    total += mark ?? 0;
                });

                // Create a cell for the total
                var totalCell = document.createElement('td');
                totalCell.textContent = total<0?'Absent':total;
                row.appendChild(totalCell);

                // Determine Pass/Fail Status
                passStatus = getPassStatus(id, result[key]);
                
                gp = getGradePoint(id, total);
                point = updateIfOptional(gp, user.subject.name, key);

                // Create a cell for the grade point
                var pointCell = document.createElement('td');
                pointCell.textContent = passStatus === 'Pass' ? gp : 0;
                row.appendChild(pointCell);

                grade = getGrade(id, total);

                // Create a cell for the grade
                var gradeCell = document.createElement('td');
                gradeCell.textContent = passStatus === 'Pass' ? grade : 'F';
                row.appendChild(gradeCell);

                // Update total GPA
                if (passStatus === 'Pass') {
                    totalPoints += point;
                } else if(user.subject.name !== key) {
                    allSubjectsPassed = false;
                }

                // Append the row to the table
                table.appendChild(row);

                totalSubjects++;
            }
        }

        // Calculate GPA
        let gpa = (allSubjectsPassed) ? totalPoints / 6 : 0;
        gpa = gpa > 5 ? 5 : gpa;

        // Display Total GPA and Overall Pass/Fail Status
        var summaryRow = document.createElement('tr');

        // Add cells for Total GPA and Overall Pass/Fail Status
        var gpaCell = document.createElement('td');
        gpaCell.colSpan = 5; // Span across the columns
        gpaCell.textContent = 'GPA: ' + gpa.toFixed(2);
        summaryRow.appendChild(gpaCell);

        var passStatusCell = document.createElement('td');
        passStatusCell.colSpan = 2; // Span across the columns
        passStatusCell.textContent = 'Status: ' + (allSubjectsPassed ? 'Pass' : 'Fail');
        passStatusCell.classList.add(allSubjectsPassed ? 'font-bold' : 'text-red-500');
        summaryRow.appendChild(passStatusCell);

        // Display Total GPA and Overall Pass/Fail Status
        var errorRow = document.createElement('tr');

        // Add cells for Total GPA and Overall Pass/Fail Status
        var errorCell = document.createElement('th');
        errorCell.colSpan = 7; // Span across the columns
        errorCell.classList.add('text-red-500');
        // errorCell.textContent = 'Error! Contact to your teachers';
        errorRow.appendChild(errorCell);

        // Append the summary row to the table
        if(totalSubjects == 7) {
            table.appendChild(summaryRow);
        } else {
            table.appendChild(errorRow);
        }

        // Append the result table to the container
        tableContainer.appendChild(table);

        // Append guardian's signature to the container
        var guardian = document.createElement('div');
        guardian.innerHTML = "<br/><br/><br/><br/><br/>-----------------------<br/>Guardian's Signature";
        guardian.classList.add('print-only');
        tableContainer.appendChild(guardian);
    }

    // Function to determine Pass/Fail Status based on the conditions
    function getPassStatus(id, subjectData) {
        if(id < 10) {
            if (subjectData.prac && subjectData.prac >=0) {
                return (subjectData.cq >= 17 && subjectData.mcq >= 8 && subjectData.prac >= 8) ? 'Pass' : 'Fail';
            } else if (subjectData.mcq && subjectData.mcq >= 0) {
                return (subjectData.cq >= 23 && subjectData.mcq >= 10) ? 'Pass' : 'Fail';
            } else {
                return (subjectData.cq >= 33) ? 'Pass' : 'Fail';
            }
        } else {
            if (subjectData.mcq && subjectData.mcq >= 0) {
                return (subjectData.cq >= 10 && subjectData.mcq >= 7) ? 'Pass' : 'Fail';
            } else {
                return (subjectData.cq >= 17) ? 'Pass' : 'Fail';
            }
        }
        
    }


    function getGradePoint(id, x) {
        let m = x;
        if(id>=10)
            m *= 2;
        if (m >= 80) {
            return 5;
        } else if (m >= 70) {
            return 4;
        } else if (m >= 60) {
            return 3.5;
        } else if (m >= 50) {
            return 3;
        } else if (m >= 40) {
            return 2;
        } else if (m >= 33) {
            return 1;
        } else {
            return 0;
        }
    }

    function getGrade(id, x) {
        let m = x;
        if(id>=10)
            m *= 2;
        if (m >= 80) {
            return 'A+';
        } else if (m >= 70) {
            return 'A';
        } else if (m >= 60) {
            return 'A-';
        } else if (m >= 50) {
            return 'B';
        } else if (m >= 40) {
            return 'C';
        } else if (m >= 33) {
            return 'D';
        } else {
            return 'F';
        }
    }
    function updateIfOptional(p, o, s) {
        if(o != s)
            return p;
        p -= 2;
        return p<0?0:p;
    }
    function notFound() {
        let container = document.getElementById('result');
        container.innerHTML = '<div class="text-red-500 border-red-500 bg-red-50 border px-4 py-2 rounded">Result Not Found</div>';
    }
    function printDiv(divId) {
        var printContents = document.getElementById(divId).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
@endsection