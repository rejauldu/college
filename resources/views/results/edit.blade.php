@extends('layouts.backend')
@section('title')
{{ __(isset($metas)?'Update meta':'Create meta') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-info">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($metas)?'Update result':'Create result') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                               <form action="{{ route('results.update', $result->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    @foreach($result  as $key => $value)
                                    @if(!in_array($key, ['id', 'exam_type_id', 'roll', 'hsc', 'created_at', 'updated_at']))
                                    <div class="mb-3 mt-3">
                                        <label for="{{ $key }}" class="form-label">{{ $key }}:</label>
                                        <input type="number" class="form-control" id="{{ $value ?? '' }}" placeholder="Enter number" name="{{ $key }}" value="{{ $value ?? '' }}" />
                                    </div>
                                    @endif
                                    @endforeach
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div><!--/col-9-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection