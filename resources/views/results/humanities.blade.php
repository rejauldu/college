@php
$id = $results[0]->exam_type_id;
$colspan_for_prac = 5;
if($id>=10) {
    $colspan_for_prac = 4;
}
@endphp
<table>
    <thead>
    <tr>
        <th colspan="2" style="font-size:16px; text-align:center; color:red; background:papayawhip">Info</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:blue; background:lightblue">Bangla</th>
        <th colspan="2" style="font-size:16px; text-align:center; color:red; background:papayawhip">English</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:blue; background:lightblue">ICT</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:red; background:papayawhip">Agriculture</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:blue; background:lightblue">Social Work</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:red; background:papayawhip">Political Science</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:blue; background:lightblue">Islamic History</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:red; background:papayawhip">Logic</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:red; background:papayawhip">Geography</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:red; background:papayawhip">Philosophy</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:red; background:papayawhip">Economics</th>
        <th rowspan="2" style="font-size:16px; text-align:center; color:blue; background:lightblue; vertical-align:middle">GPA</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td style="font-size:12px; text-align:center; background:silver">Roll</td>
            <td style="font-size:12px; text-align:center; background:silver">Name</td>
            <!--Bangla-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--English-->
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--ICT-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Agriculture-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Social Work-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Political Science-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Islamic History-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Logic-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Geography-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Philosophy-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Economics-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
        </tr>
    @php
    $t = 0;
    $i = 0;
    $optional = 0;
    @endphp

    @foreach($results as $r)
        @php
            $arr = [];
            $i++;
        @endphp
        <tr>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->roll }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif @if(!$r->user) color:red; @endif">{{ $r->user->name ?? 'No User'}}</td>
            <!--Bangla-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->bangla_cq == -1?'A':$r->bangla_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->bangla_mcq == -1?'A':$r->bangla_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->bangla_cq !== null)
                    @php
                        $t = getTotal($id, $r->bangla_cq, $r->bangla_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->bangla_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--English-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @php
                    $t = getTotal($id, $r->english);
                @endphp
                @if($r->english !== null && $r->english == -1)
                    A
                @else
                {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->english !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--ICT-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_cq == -1?'A':$r->ict_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_mcq == -1?'A':$r->ict_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_prac == -1?'A':$r->ict_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->ict_cq !== null)
                    @php
                        $t = getTotal($id, $r->ict_cq, $r->ict_mcq, $r->ict_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->ict_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Agriculture-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_cq == -1?'A':$r->agri_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_mcq == -1?'A':$r->agri_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_prac == -1?'A':$r->agri_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->agri_cq !== null)
                    @php
                        $t = getTotal($id, $r->agri_cq, $r->agri_mcq, $r->agri_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->agri_cq !== null)
                    @if($r->user && $r->user->subject_id == 8)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Social Work-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->social_cq == -1?'A':$r->social_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->social_mcq == -1?'A':$r->social_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->social_cq !== null)
                    @php
                        $t = getTotal($id, $r->social_cq, $r->social_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->social_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Political Science-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->political_cq == -1?'A':$r->political_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->political_mcq == -1?'A':$r->political_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->political_cq !== null)
                    @php
                        $t = getTotal($id, $r->political_cq, $r->political_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->political_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Islamic History-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->islamic_cq == -1?'A':$r->islamic_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->islamic_mcq == -1?'A':$r->islamic_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->islamic_cq !== null)
                    @php
                        $t = getTotal($id, $r->islamic_cq, $r->islamic_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->islamic_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Logic-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->logic_cq == -1?'A':$r->logic_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->logic_mcq == -1?'A':$r->logic_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->logic_cq !== null)
                    @php
                        $t = getTotal($id, $r->logic_cq, $r->logic_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->logic_cq !== null)
                    @if($r->user && $r->user->subject_id == 12)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Geography-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->geography_cq == -1?'A':$r->geography_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->geography_mcq == -1?'A':$r->geography_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->geography_prac == -1?'A':$r->geography_mcq }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->geography_cq !== null)
                    @php
                        $t = getTotal($id, $r->geography_cq, $r->geography_mcq, $r->geography_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->geography_cq !== null)
                    @if($r->user && $r->user->subject_id == 13)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Philosophy-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->philosophy_cq == -1?'A':$r->philosophy_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->philosophy_mcq == -1?'A':$r->philosophy_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->philosophy_cq !== null)
                    @php
                        $t = getTotal($id, $r->philosophy_cq, $r->philosophy_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->philosophy_cq !== null)
                    @if($r->user && $r->user->subject_id == 18)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Economics-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->economics_cq == -1?'A':$r->economics_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->economics_mcq == -1?'A':$r->economics_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->economics_cq !== null)
                    @php
                        $t = getTotal($id, $r->economics_cq, $r->economics_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->economics_cq !== null)
                    @if($r->user && $r->user->subject_id == 17)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif font-weight:bold;">
                @if(count($arr) == 6 && $r->user && in_array($r->user->subject_id, [8, 12, 13, 17, 18]))
                    {{ getGpa($arr, $optional) }}
                @else
                    Invalid
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@php
function getTotal($id, $cq=0, $mcq=null, $prac=null) {
    if($id<10 && $prac !== null) {
        if($cq>=17 && $mcq >= 8 && $prac >= 8)
            return $cq+$mcq+$prac;
        else
            return 0;
    }
    if($id<10) {
        if($mcq !== null) {
            if($cq>=23 && $mcq >= 10)
                return $cq+$mcq;
            else
                return 0;
        }
        if($cq>=33)
            return $cq;
        else
            return 0;
    } else {
        if($mcq !== null) {
            if($cq>=10 && $mcq >= 7)
                return $cq+$mcq;
            else
                return 0;
        }
        if($cq>=17)
            return $cq;
        else
            return 0;
    }
}
function getGp($id, $t=0) {
    if($id>=10)
        $t*=2;
    if($t >= 80)
        return 5;
    elseif($t >= 70)
        return 4;
    elseif($t >= 60)
        return 3.5;
    elseif($t >= 50)
        return 3;
    elseif($t >= 40)
        return 2;
    elseif($t >= 33)
        return 1;
    else
        return 0;
}
function getOptional($gp) {
    if($gp >= 2)
        return $gp-2;
    else
        return 0;
}
function getGpa($arr, $optional) {
    $total = 0;
    foreach($arr as $a) {
        if($a<1)
            return "F";
        else
            $total+=$a;
    }
    $total += $optional;
    if($total/6 > 5)
        return 5;
    else
        return number_format($total/6, 2, '.', '');
}
@endphp