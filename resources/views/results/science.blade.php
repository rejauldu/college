@php
$id = $results[0]->exam_type_id;
$colspan_for_prac = 5;
if($id>=10) {
    $colspan_for_prac = 4;
}
@endphp
<table>
    <thead>
    <tr>
        <th colspan="2" style="font-size:16px; text-align:center; color:red; background:papayawhip">Info</th>
        <th colspan="4" style="font-size:16px; text-align:center; color:blue; background:lightblue">Bangla</th>
        <th colspan="2" style="font-size:16px; text-align:center; color:red; background:papayawhip">English</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:blue; background:lightblue">ICT</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:red; background:papayawhip">Agriculture</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:blue; background:lightblue">Physics</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:red; background:papayawhip">Chemistry</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:blue; background:lightblue">Higher Math</th>
        <th colspan="{{ $colspan_for_prac }}" style="font-size:16px; text-align:center; color:red; background:papayawhip">Biology</th>
        <th rowspan="2" style="font-size:16px; text-align:center; color:blue; background:lightblue; vertical-align:middle">GPA</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td style="font-size:12px; text-align:center; background:silver">Roll</td>
            <td style="font-size:12px; text-align:center; background:silver">Name</td>
            <!--Bangla-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--English-->
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--ICT-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Agriculture-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Physics-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Chemistry-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Math-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
            <!--Biology-->
            <td style="font-size:12px; text-align:center; background:silver">CQ</td>
            <td style="font-size:12px; text-align:center; background:silver">MCQ</td>
            @if($id<10)
            <td style="font-size:12px; text-align:center; background:silver">Practical</td>
            @endif
            <td style="font-size:12px; text-align:center; background:silver">Total</td>
            <td style="font-size:12px; text-align:center; background:silver">GP</td>
        </tr>
    @php
    $t = 0;
    $i = 0;
    $optional = 0;
    @endphp

    @foreach($results as $r)
        @php
            $arr = [];
            $i++;
        @endphp
        <tr>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->roll }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif @if(!$r->user) color:red; @endif">{{ $r->user->name ?? 'No User'}}</td>
            <!--Bangla-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->bangla_cq == -1?'A':$r->bangla_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->bangla_mcq == -1?'A':$r->bangla_mcq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->bangla_cq !== null)
                    @php
                        $t = getTotal($id, $r->bangla_cq, $r->bangla_mcq);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->bangla_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--English-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @php
                    $t = getTotal($id, $r->english);
                @endphp
                @if($r->english !== null && $r->english == -1)
                    A
                @else
                {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->english !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--ICT-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_cq == -1?'A':$r->ict_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_mcq == -1?'A':$r->ict_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->ict_prac == -1?'A':$r->ict_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->ict_cq !== null)
                    @php
                        $t = getTotal($id, $r->ict_cq, $r->ict_mcq, $r->ict_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->ict_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Agriculture-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_cq == -1?'A':$r->agri_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_mcq == -1?'A':$r->agri_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->agri_prac == -1?'A':$r->agri_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->agri_cq !== null)
                    @php
                        $t = getTotal($id, $r->agri_cq, $r->agri_mcq, $r->agri_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->agri_cq !== null)
                    @if($r->user && $r->user->subject_id == 8)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Physics-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->physics_cq == -1?'A':$r->physics_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->physics_mcq == -1?'A':$r->physics_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->physics_prac == -1?'A':$r->physics_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->physics_cq !== null)
                    @php
                        $t = getTotal($id, $r->physics_cq, $r->physics_mcq, $r->physics_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->physics_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Chemistry-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->chemistry_cq == -1?'A':$r->chemistry_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->chemistry_mcq == -1?'A':$r->chemistry_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->chemistry_prac == -1?'A':$r->chemistry_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->chemistry_cq !== null)
                    @php
                        $t = getTotal($id, $r->chemistry_cq, $r->chemistry_mcq, $r->chemistry_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->chemistry_cq !== null)
                    {{ $arr[] = getGp($id, $t) }}
                @endif
            </td>
            <!--Math-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->math_cq == -1?'A':$r->math_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->math_mcq == -1?'A':$r->math_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->math_prac == -1?'A':$r->math_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->math_cq !== null)
                    @php
                        $t = getTotal($id, $r->math_cq, $r->math_mcq, $r->math_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->math_cq !== null)
                    @if($r->user && $r->user->subject_id == 6)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <!--Biology-->
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->biology_cq == -1?'A':$r->biology_cq }}</td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->biology_mcq == -1?'A':$r->biology_mcq }}</td>
            @if($id<10)
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">{{ $r->biology_prac == -1?'A':$r->biology_prac }}</td>
            @endif
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->biology_cq !== null)
                    @php
                        $t = getTotal($id, $r->biology_cq, $r->biology_mcq, $r->biology_prac);
                    @endphp
                    {{ $t==0?'F':$t }}
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif">
                @if($r->biology_cq !== null)
                    @if($r->user && $r->user->subject_id == 7)
                    @php
                    $gp = getGp($id, $t);
                    $optional = getOptional($gp);
                    @endphp
                    {{ $gp }}
                    @else
                    {{ $arr[] = getGp($id, $t) }}
                    @endif
                @endif
            </td>
            <td style="text-align:center; @if($i%2==0) background:lightgray; @endif font-weight:bold;">
                @if(count($arr) == 6 && $r->user && in_array($r->user->subject_id, [6, 7, 8]))
                    {{ getGpa($arr, $optional) }}
                @else
                    Invalid
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@php
function getTotal($id, $cq=0, $mcq=null, $prac=null) {
    if($id<10 && $prac !== null) {
        if($cq>=17 && $mcq >= 8 && $prac >= 8)
            return $cq+$mcq+$prac;
        else
            return 0;
    }
    if($id<10) {
        if($mcq !== null) {
            if($cq>=23 && $mcq >= 10)
                return $cq+$mcq;
            else
                return 0;
        }
        if($cq>=33)
            return $cq;
        else
            return 0;
    } else {
        if($mcq !== null) {
            if($cq>=10 && $mcq >= 7)
                return $cq+$mcq;
            else
                return 0;
        }
        if($cq>=17)
            return $cq;
        else
            return 0;
    }
}
function getGp($id, $t=0) {
    if($id>=10)
        $t*=2;
    if($t >= 80)
        return 5;
    elseif($t >= 70)
        return 4;
    elseif($t >= 60)
        return 3.5;
    elseif($t >= 50)
        return 3;
    elseif($t >= 40)
        return 2;
    elseif($t >= 33)
        return 1;
    else
        return 0;
}
function getOptional($gp) {
    if($gp >= 2)
        return $gp-2;
    else
        return 0;
}
function getGpa($arr, $optional) {
    $total = 0;
    foreach($arr as $a) {
        if($a<1)
            return "F";
        else
            $total+=$a;
    }
    $total += $optional;
    if($total/6 > 5)
        return 5;
    else
        return number_format($total/6, 2, '.', '');
}
@endphp