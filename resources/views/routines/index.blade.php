@extends('layouts.app')
@section('title')
Class Routine | ক্লাস রুটিন
@endsection
@section('content')
<div class="container mx-auto">
    <div class="">
        <div class="swiper routine-slider">
            <div class="swiper-wrapper">
                @foreach($posts as $post)
                <div class="swiper-slide">
                    <div class="swiper-zoom-container">
                        <img
                            src="/posts/{{ $post->link ?? 'placeholder.webp' }}"
                            width="1000"
                            height="562"
                            alt="Routine of {{ $post->title ?? '' }}"
                            class="w-full rounded-lg" />
                    </div>
                    <p class="absolute w-full text-center top-0 text-sm md:text-xl text-white">
                        <span class="px-1 sm:p-1 md:p-3 bg-[#000A] rounded inline-block">{{ $post->title ?? '' }}</span>
                    </p>
                    <p class="text-center"><a href="/posts/{{ $post->link ?? 'placeholder.webp' }}" class="text-theme-primary border-theme-primary px-1 sm:p-1 md:p-3 border rounded inline-block underline text-sm md:text-3xl">ফুল স্ক্রিন দেখতে এখানে ক্লিক করুন</a></p>
                </div>
                @endforeach
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</div>
@endsection