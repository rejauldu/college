<h2 class="text-center mt-3">Academic Calendar</h2>
<div class="container mx-auto my-3">
    <table class="table">
        <tr>
            <th class="border border-gray-300 p-2">Sun</th>
            <th class="border border-gray-300 p-2">Mon</th>
            <th class="border border-gray-300 p-2">Tue</th>
            <th class="border border-gray-300 p-2">Wed</th>
            <th class="border border-gray-300 p-2">Thu</th>
            <th class="border border-gray-300 p-2">Fri</th>
            <th class="border border-gray-300 p-2">Sat</th>
        </tr>
        @foreach($dates->chunk(7) as $week)
        @php
        $i=0;
        @endphp
            <tr>
                @foreach($week as $date)
                    @php
                        $i++;
                    @endphp
                    
                    <td
                        class="border border-gray-300 p-2 text-center @if(($date && $date->status == 0) || ($i == 6 || $i == 7)) bg-red-50 text-red-500 @endif">
                        <!-- Tooltip wrapper -->
                        <div class="tooltip-wrapper group">
                            <span class="cursor-pointer">{{ $date->id ?? "" }}</span>
    
                            <!-- Tooltip content -->
                            <div class="absolute left-1/2 -translate-x-1/2 bottom-full mb-2 w-40 p-2 bg-gray-800 text-white text-sm rounded hidden group-hover:block group-focus:block">
                                @if(($date && $date->status == 0) || ($i == 6 || $i == 7)) কলেজ বন্ধ। @endif{{ $date->note ?? "" }}
                            </div>
                        </div>
                        
                    </td>
                    
                @endforeach
            </tr>
        @endforeach
    </table>
</div>