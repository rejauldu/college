<div class="container mx-auto">
    <div class="grid md:grid-cols-[auto_1fr_auto]">
        <div class="lg:p-3 hidden md:block">
            @if(isset($principal))
            <div class="text-center max-w-[13.3rem] border rounded-lg overflow-hidden hidden lg:grid">
                <p class="p-3 bg-theme-primary text-white">Principal's Message</p>
                <img
                    src="/images/profile/{{ $principal->photo ?? 'placeholder.webp' }}"
                    width="215"
                    height="270"
                    alt="Principal of Hasanpur College"
                />
                <div class="py-3">
                    <p class="text-lg font-bold">{{ $principal->meta->designation ?? '' }} {{ $principal->name ?? '' }}</p>
                    <p class="text-sm text-gray-600">Principal</p>
                    <a href="{{ route('app.principal') }}" class="text-red-500">Continue reading...</a>
                </div>
            </div>
            @endif
        </div>
        <div class="lg:p-3">
            <div class="swiper normal-slider">
                <h1 class="absolute left-0 right-0 w-full text-center text-sm  md:text-xl bg-black text-white rounded md:p-1 drop-shadow z-10 capitalize">{{ config('app.name') }}</h1>
                <div class="swiper-wrapper">
                    @foreach($slides as $slide)
                    <div class="swiper-slide relative">
                        <img src="/posts/{{ $slide->link ?? 'placeholder.webp' }}" width="1000" height="562" alt="{{ $slide->title ?? '' }}" class="w-full rounded-lg" />
                        <p class="absolute left-0 right-0 bottom-0 w-full text-center text-sm md:text-xl bg-[#0008] text-white rounded md:p-1 drop-shadow">{{ $slide->title ?? '' }}</p>
                    </div>
                    @endforeach
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
        <div class="p-3 hidden md:block">
            @if(isset($vice))
            <div class="text-center max-w-[13.3rem] border rounded-lg overflow-hidden hidden lg:grid">
                <p class="p-3 bg-theme-primary text-white">Vice Principal's Message</p>
                <img
                    src="/images/profile/{{ $vice->photo ?? 'placeholder.webp' }}"
                    width="215"
                    height="270"
                    alt="Vice Principal of Hasanpur College"
                />
                <div class="py-3">
                    <p class="text-lg font-bold">{{ $vice->meta->designation ?? '' }} {{ $vice->name ?? '' }}</p>
                    <p class="text-sm text-gray-600">Vice Principal</p>
                    <a href="{{ route('app.vice') }}" class="text-red-500">Continue reading...</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>