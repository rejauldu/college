<div class="container mx-auto">
    <div class="py-3 rounded-lg border-2 border-theme-primary text-theme-primary text-xl my-3">
        <marquee onmouseover="this.stop();" onmouseout="this.start();">
            @foreach($movings as $moving)
                @if($moving->metas->count() && $moving->metas[0]->meta_value) <a href="{{ route('notices.show', $moving->id) }}/{{ urlencode($moving->title) }}">{{ $moving->title ?? '' }}</a> | @endif
            @endforeach
        </marquee>
    </div>
    <div class="grid lg:grid-cols-[auto_1fr_auto]">
        <div class="hidden lg:block">
            @include('layouts.info')
        </div>
        <div class="lg:px-3">
            <h2 class="text-3xl bg-theme-primary text-white text-center p-3 rounded-t">নোটিশ বোর্ড</h2>
            <div class="w-full overflow-y-auto">
                <table class="table mb-5 bg-blue-50 !text-left">
                @foreach($notices as $notice)
                    <tr>
                        <td><a href="{{ route('notices.show', $notice->id) }}/{{ urlencode($notice->title) }}">{{ $notice->title ?? '' }}</a></td>
                        <td class="text-gray-500">{{ $notice->created_at->format('jS M') ?? '' }}</td>
                    </tr>
                @endforeach
                </table>
                <div>
                    {{ $notices->links() }}
                </div>
            </div>
        </div>
        <div class="hidden lg:block">
            @include('layouts.subject')
        </div>
    </div>
</div>