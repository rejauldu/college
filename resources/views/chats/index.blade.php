@extends('layouts.noFooter')
@section('title')
Group Study Hasanpur Shahid Nazrul Government College
@endsection
@section('content')
<div class="container mx-auto h-screen grid grid-rows-[1fr_9rem] overflow-y-auto" id="content">
    <div class="grid gap-1">
        @php
            $auth = auth()->user()
        @endphp
        @foreach($posts as $post)
            <div class="grid gap-1 p-2 content-start @if($post->user->id == $auth->id) rounded-s-3xl justify-self-end bg-blue-100 @else rounded-r-3xl justify-self-start bg-gray-200  @endif w-[90%] md:max-w-[80%] overflow-hidden">
                <div class="flex gap-1">
                    <img class="w-5 h-5 rounded-full" src="/images/profile/{{ $post->user->photo ?? 'avatar.webp' }}" />
                    <a class=" font-bold" href="{{ route('users.show', $post->user->id) }}/{{ urlencode($post->user->name) }}">{{ $post->user->name }}</a>
                </div>
                <div>
                    {!! clickable($post->content ?? '') !!}
                    <span class="float-right text-gray-500 text-sm">
                        @php
                            $diff = str_replace(' from now', '', $post->created_at->shortRelativeDiffForHumans());
                            $diff = str_replace(' ago', '', $diff);
                        @endphp
                        {{ $diff }}
                    </span>
                </div>
            </div>
        @endforeach
    </div>
    <!-- Search Form -->
    <div class="">
        <form action="{{ route('chats.store') }}" class="flex gap-0 mt-2 mb-1 fixed inset-0 left-3 right-3 top-auto" method="post">
            @csrf
            <input
                id="input"
                type="text" 
                name="content" 
                placeholder="Write..."
                class=""
                autofocus
            >
            <button class="pl-3 rounded-s-none rounded-r-lg flex items-center border px-3 bg-gray-100 text-theme-primary" type="submit">
                Send
            </button>
        </form>
    </div>
</div>
@endsection
@section('script')
<script>
function scrollToBottom() {
    let content = document.getElementById("content");
    content.scrollTop = content.scrollHeight;
}
scrollToBottom();
function reloadPageEveryInterval(intervalInSeconds, maxReloads) {
    // Get the current reload count from localStorage, or initialize it to 0 if not set
    let reloadCount = parseInt(localStorage.getItem('reloadCount')) || 0;
    const intervalInMilliseconds = intervalInSeconds * 1000;

    const intervalId = setInterval(() => {
        if (reloadCount < maxReloads) {
            reloadCount++;
            localStorage.setItem('reloadCount', reloadCount);
            let input = document.getElementById("input");
            if(input.value.length == 0)
                location.reload();
        } else {
            clearInterval(intervalId); // Stop the interval after the maximum reloads
            localStorage.removeItem('reloadCount');
        }
    }, intervalInMilliseconds);
}

// Call the function to reload the page every 10 seconds for 100 times
reloadPageEveryInterval(15, 30);
</script>
@endsection
@php
function clickable($text) {
    // Regular expression to find URLs in the text
    $urlPattern = '/(https?:\/\/[^\s]+)/';
    
    // Replace URLs with anchor tags
    $textWithLinks = preg_replace($urlPattern, '<a class="text-blue-500" href="$1" target="_blank">$1</a>', $text);
    
    return $textWithLinks;
}
@endphp