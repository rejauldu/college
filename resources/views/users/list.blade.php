@extends('layouts.backend')
@section('title')
{{ __('All Users') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="nav-icon fas fa-th mr-1"></i> {{ __('User List') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <form action="{{ route('users.list') }}">
                                <div class="form-group d-flex">
                                    <label for="phone">Phone:</label>
                                    <input type="text" name="phone" class="form-control ml-2" value="{{ $phone ?? '' }}" id="phone">
                                </div>
                            </form>
                            <form action="{{ route('users.list') }}">
                                <div class="form-group d-flex">
                                    <label for="roll">Roll:</label>
                                    <input type="text" name="roll" class="form-control ml-2" value="{{ $roll ?? '' }}" autofocus id="roll">
                                </div>
                            </form>
                        </div>
                        <table id="dataTables" class="display nowrap mb-5" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>HSC</th>
                                    <th>Roll</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->hsc }}</td>
                                    <td>{{ $user->roll }}</td>
                                    <td>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="user{{ $user->id }}" onchange="updateStatus(event, {{ $user->id }})" @if(isset($user->status) && $user->status == 1) checked @endif>
                                            <label class="custom-control-label" for="user{{ $user->id }}"></label>
                                        </div>
                                    </td>
                                    <td><a href="{{ route('users.edit', $user->id) }}" class="text-success fa fa-edit"></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>HSC</th>
                                    <th>Roll</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="d-flex justify-content-center">{{ $users->links('pagination::bootstrap-4') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<!-- dataTables plugin -->
<link href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css" rel="stylesheet" media="all"/>
<link href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/buttons/2.4.2/css/buttons.dataTables.min.css" rel="stylesheet" media="all"/>
<!-- /dataTables plugin -->
@endsection
@section('script')
<script>
    function updateStatus(e, id) {
        console.log("status");
        const checked = e.target.checked?1:0;
        fetch('/api/user/status', {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": $('input[name="_token"]').val()
            },
            method: 'POST',
            body: JSON.stringify({
                id: id,
                status: checked
            })
            
        })
    }
</script>
<!---dataTables plugin JavaScript -->
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="{{ asset('js/dataTables.js') }}"></script>
<!--/dataTables plugin JavaScript -->
@endsection