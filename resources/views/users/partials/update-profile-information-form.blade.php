<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">
            {{ __('Profile Information') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600">
            {{ __("Update your account's profile information and email address.") }}
        </p>
    </header>

    <form method="post" action="{{ route('users.update', $user->id) }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div>
            <label for="name">{{ __('Name') }}</label>
            <input id="name" name="name" type="text" class="mt-1 block w-full" value="{{ old('name', $user->name) }}" required autofocus autocomplete="name" />
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div>
            <label for="phone">{{ __('Phone') }}</label>
            <input id="phone" name="phone" type="tel" class="mt-1 block w-full" value="{{ old('phone', $user->phone) }}" required autocomplete="phone" />
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        @student
        <div>
            <label for="roll">{{ __('Roll') }}</label>
            <input id="roll" name="roll" type="number" class="mt-1 block w-full" value="{{ old('roll', $user->roll) }}" required autocomplete="roll" />
            @error('roll')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div>
            <label for="hsc">{{ __('HSC') }}</label>
            <input id="hsc" name="hsc" type="number" class="mt-1 block w-full" value="{{ old('hsc', $user->hsc) }}" required autocomplete="hsc" />
            @error('hsc')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        @endif
        @teacher
        <div>
            <label for="bcs">{{ __('BCS') }}</label>
            <input id="bcs" name="bcs" type="number" class="mt-1 block w-full" value="{{ old('bcs', $user->bcs) }}" autocomplete="bcs" />
            @error('bcs')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        @endif
        <div>
            <label for="subject_id">{{ __('Subject') }}</label>
            <select name="subject_id" class="form-control mb-3">
                @foreach($subjects as $s)
                <option value="{{ $s->id }}" @if($user->subject_id == $s->id) selected @endif>{{ $s->name ?? '' }}</option>
                @endforeach
            </select>
            @error('subject_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div>
            <label for="role">{{ __('Account Type') }}</label>
            <select name="role" class="form-control mb-3">
                <option value="1" @if($user->role == 1) selected @endif>General User</option>
                <option value="2" @if($user->role == 2) selected @endif>Student</option>
                <option value="3" @if($user->role == 3) selected @endif>Stuff</option>
                <option value="4" @if($user->role == 4) selected @endif>Teacher</option>
                <option value="5" @if($user->role == 5) selected @endif>Vice Principal</option>
                <option value="6" @if($user->role == 6) selected @endif>Principal</option>
                <option value="7" @if($user->role == 7) selected @endif>Super Admin</option>
            </select>
            @error('subject_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div>
            <label for="note">{{ __('Note') }}</label>
            <textarea id="note" name="note" class="form-control mb-3" autocomplete="note">{{ old('note', $user->note) }}</textarea>
            @error('bcs')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="flex items-center gap-4">
            <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
            @if (session('status') === 'profile-updated')
                <p class="text-sm text-gray-600">{{ __('Saved.') }}</p>
            @endif
        </div>
    </form>
</section>
