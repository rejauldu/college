<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">
            {{ __('Other Information') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600">
            {{ __("Update your account's meta information") }}
        </p>
    </header>

    <form method="post" action="{{ route('users.meta', $user->id) }}" class="mt-6 space-y-6">
        @csrf
        @method('put')

        <div>
            <label for="name">{{ __('Designation') }}</label>
            <input id="designation" name="designation" type="text" class="mt-1 block w-full" value="{{ $user->meta->designation ?? '' }}" required autofocus autocomplete="designation" />
            @error('designation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="flex items-center gap-4">
            <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
            @if (session('status') === 'profile-updated')
                <p class="text-sm text-gray-600">{{ __('Saved.') }}</p>
            @endif
        </div>
    </form>
</section>
