<form method="post" action="{{ route('users.update', $user->id) }}" class="mt-6 space-y-6" enctype="multipart/form-data">
    @csrf
    @method('patch')
    <img src="/images/profile/{{ $user->photo }}" class="w-16 h-16 rounded-full p-1 border overflow-hidden" />
    <div>
        <label for="photo">{{ __('Photo') }} (210x210) </label>
        <input id="photo" name="photo" type="file" class="mt-1 block w-full" required/>
        @error('photo')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="flex items-center gap-4">
        <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
        @if (session('status') === 'profile-updated')
            <p class="text-sm text-gray-600">{{ __('Saved.') }}</p>
        @endif
    </div>
</form>