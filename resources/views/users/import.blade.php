@extends('layouts.backend')
@section('title')
{{ __(isset($metas)?'Update meta':'Create meta') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($metas)?'Update meta':'Create meta') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-card-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-card-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                               <form action="{{ route('users.upload') }}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3 mt-3">
                                        <label for="file" class="form-label">Excel file:</label>
                                        <input type="file" class="form-control" id="file" placeholder="Enter Users" name="users" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
                                    </div>
                                    <button type="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
                                </form>
                            </div><!--/col-9-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('style')
<link href="https://code.jquery.com/ui/1.13.0/themes/base/jquery-ui.min.css" rel="stylesheet">
@endsection
@section('script')
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker();
    } );
</script>
@endsection