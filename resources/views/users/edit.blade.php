@extends('layouts.backend')
@section('title')
{{ __('Profile') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="nav-icon fas fa-th mr-1"></i> {{ __('Profile Update') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-photo-tab" data-toggle="pill" href="#custom-tabs-one-photo" role="tab" aria-controls="custom-tabs-one-photo" aria-selected="true">Photo</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-password-tab" data-toggle="pill" href="#custom-tabs-one-password" role="tab" aria-controls="custom-tabs-one-password" aria-selected="false">Password</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-detail-tab" data-toggle="pill" href="#custom-tabs-one-detail" role="tab" aria-controls="custom-tabs-one-detail" aria-selected="false">Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-other-tab" data-toggle="pill" href="#custom-tabs-one-other" role="tab" aria-controls="custom-tabs-one-other" aria-selected="false">Other</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <div class="tab-pane fade active show" id="custom-tabs-one-photo" role="tabpanel" aria-labelledby="custom-tabs-one-photo-tab">
                                    @include('users.partials.update-photo-form')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-password" role="tabpanel" aria-labelledby="custom-tabs-one-password-tab">
                                    @include('users.partials.update-password-form')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-detail" role="tabpanel" aria-labelledby="custom-tabs-one-detail-tab">
                                    @include('users.partials.update-profile-information-form')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-other" role="tabpanel" aria-labelledby="custom-tabs-one-other-tab">
                                    @include('users.partials.update-other-form')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection