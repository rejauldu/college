@extends('layouts.app')
@section('title')
Teachers of Hasanpur S N Govt. College
@endsection
@section('content')
<div class="container mx-auto">
    <h1 class="text-xl md:text-3xl text-center my-5 md:mt-10">Faculty Members</h1>
    <div class="grid gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5">
        @foreach($users as $user)
            <a href="{{ route('users.show', $user->id) }}/{{ urlencode($user->name) }}" class="grid items-between text-center border bg-gray-50 rounded-xl hover:no-underline hover:text-black hover:scale-105 transition-all duration-300 hover::shadow-xl">
                <img width="210" height="210" alt="{{ $user->name }}" class="rounded-full p-4 mx-auto w-full object-cover max-w-[10rem] max-h-[10rem]" src="/images/profile/{{ $user->photo ?? 'avatar.webp' }}" />
                <div class="bg-white p-5">
                    <h5>{{ $user->name ?? '' }}</h5>
                    <p>{{ $user->meta->designation ?? 'Lecturer' }} ({{ $user->subject->name ?? '' }})</p>
                    @if($user->bcs)
                    <p>BCS (General Education) Cadre, {{ $user->bcs ?? '' }}<sup>{{ ordinal($user->bcs) }}</sup> BCS</p>
                    @endif
                </div>
            </a>
        @endforeach
    </div>
</div>
@endsection