@extends('layouts.app')
@section('title')
About Hasanpur Shahid Nazrul Government College
@endsection
@section('content')
<div class="container mx-auto">
    <div class="overflow-x-auto w-[calc(100vw_-_2rem)]">
        <table class="table !text-left">
            @foreach($users as $user)
                <tr>
                    <td class="w-20 sm:w-28 md:w-40"><img class="rounded p-1 border" src="/images/profile/{{ $user->photo ?? 'avatar.webp' }}" /></td>
                    <td>
                        <div>
                            <h5>{{ $user->name ?? '' }}</h5>
                            <p>{{ $user->meta->designation ?? '' }} ({{ $user->subject->name ?? '' }})</p>
                            <p>BCS (General Education) Cadre, {{ $user->meta->bcs ?? '' }}<sup>th</sup> BCS</p>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection