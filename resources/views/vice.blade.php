@extends('layouts.app')
@section('title')
vice Principal's Message
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid grid-cols-[auto_1fr] gap-1 md:gap-5 mb-3 md:mb-5 border-b p-3 md:p-5 bg-theme-light">
        <img
            src={{ url("/images/profile/$user->photo") }}
            width="215"
            height="270"
            alt="{{ $user->name }}"
            class="border p-1 rounded max-w-[15rem]"
        />
        <div class="grid gap-1 content-center">
            <h1><a href="{{ route("users.show", $user->id) }}/{{ urlencode($user->name) }}">{{ $user->name }}</a></h1>
            <h4 class="text-lg">Vice Principal</h4>
            <p class="text-gray-600">{{ config("app.name") }}</p>
        </div>
    </div>
    <div class="grid gap-3 mb-5 text-justify">{!! $post->body !!}</div>
    <p>Warm Regards,</p>
    <p class="text-xl">{{ $user->name }}</p>
    <h4 class="text-lg">Vice Principal</h4>
    <p class="text-gray-500 ">{{ config("app.name") }}</p>
</div>
@endsection