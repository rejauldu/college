@extends('layouts.app')
@section('title')
{{ $post->title ?? '' }}
@endsection
@section('content')
<div class="container mx-auto min-h-screen">
    <div class="grid lg:grid-cols-[auto_1fr_auto] py-3">
        <div class="hidden lg:block">
            @include('layouts.info')
        </div>
        <div class="lg:px-3">
            <h1 class="text-3xl capitalize">{{  $post->title ?? '' }}</h1>
            <div class="mb-3">
                <p class="text-gray-500 text-sm">Posted at: {{ $post->created_at->format('jS M Y') }}</p>
            </div>
            <div class="mb-3">
                {{  $post->body ?? '' }}
            </div>
            @php
            $pathtofile = '/posts/'.$post->link;
            $info = pathinfo($pathtofile);
            @endphp
            @if (isset($info["extension"]) && $info["extension"] == "pdf")
                <object data="/posts/{{ $post->link }}" type="application/pdf" width="600" height="500" class="w-full overflow-hidden border-4">
                    <p class="bg-theme-light p-3 rounded"><a class="underline" href="/posts/{{ $post->link }}" download>{{ $post->title ?? 'Download' }}</a></p>
                    <embed src="/posts/{{ $post->link }}" width="100%" height="600px"/>
                </object>
            @else
                <img class="w-full" src="/posts/{{ $post->link }}" alt="{{ $post->title ?? '' }}" />
            @endif
        </div>
        <div class="hidden lg:block">
            @include('layouts.subject')
        </div>
    </div>
</div>
@endsection