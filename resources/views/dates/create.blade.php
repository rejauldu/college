@extends('layouts.backend')
@section('title')
{{ __(isset($post)?'Update post':'Create post') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($post)?'Update Date':'Add Date') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                                <form action="@if(isset($post)) {{ route('dates.update', $post->id) }} @else {{ route('dates.store') }} @endif" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($post))
                                    @method('PUT')
                                    @endif
                                    @if(isset($post))
                                    <p class="alert alert-danger"><b>Date:</b>{{ $post->id }}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="note">Note</label>
                                        <input id="note" type="text" class="form-control" name="note" value="{{ $post->note ?? '' }}" placeholder="Note" title="Enter note" />
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="status" value="1" @if(isset($post) && $post->status == 1) checked @endif>Closed
                                        </label>
                                    </div>
                                    <div class="form-group mt-3">
                                        <button class="btn btn-success btn-theme" type="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection