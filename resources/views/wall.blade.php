@extends('layouts.app')
@section('title')
Wall of Hasanpur Shahid Nazrul Government College
@endsection
@section('content')
<div class="container mx-auto">
    <!-- Search Form -->
    <div class="">
        <form action="{{ route('status.store') }}" class="border overflow-hidden border-gray-400 rounded mt-4 focus-within:outline outline-1 outline-theme-primary flex gap-0 mb-1 items-start bg-gray-100" method="post" enctype='multipart/form-data' id="post">
            @csrf
            <div class="relative inline-block py-1 bg-blue-200" id="file">
                <div class="flex items-center justify-center p-2 cursor-pointer">
                    <svg class="w-6 h-6 text-blue-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M12 2l5.5 5.5h-3v4h-5v-4h-3L12 2z" />
                        <path d="M12 15.5c-1.4 0-2.6-.6-3.5-1.5l-1.4 1.4c1.3 1.4 3 2.1 4.9 2.1 1.9 0 3.6-.7 4.9-2.1l-1.4-1.4c-.9.9-2.1 1.5-3.5 1.5z" />
                        <path d="M3 13v4h18v-4H3z" />
                    </svg>
                </div>
                <input type="file" name="link" class="absolute inset-0 w-full h-full opacity-0 cursor-pointer" onchange="preview(event)" />
            </div>
            <textarea
                name="body"
                placeholder="স্ট্যাটাস লিখুন..."
                rows="1"
                class="grow resize-none overflow-hidden focus:ring-0 focus:ring-offset-0 border-0 relative bg-transparent"
                id="textarea"
            ></textarea>
            <button class="p-2 text-blue-500 hover:text-orange-500 transition-all duration-300" type="submit">
                <svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor" d="M498.1 5.6c10.1 7 15.4 19.1 13.5 31.2l-64 416c-1.5 9.7-7.4 18.2-16 23s-18.9 5.4-28 1.6L284 427.7l-68.5 74.1c-8.9 9.7-22.9 12.9-35.2 8.1S160 493.2 160 480V396.4c0-4 1.5-7.8 4.2-10.7L331.8 202.8c5.8-6.3 5.6-16-.4-22s-15.7-6.4-22-.7L106 360.8 17.7 316.6C7.1 311.3 .3 300.7 0 288.9s5.9-22.8 16.1-28.7l448-256c10.7-6.1 23.9-5.5 34 1.4z"/>
                </svg>
            </button>
        </form>
        <div id="preview"></div>
    </div>
    <div class="grid gap-1">
        @php
            $auth = auth()->user()
        @endphp
        @foreach($posts as $post)
            <div class="border mb-2 shadow bg-blue-100 overflow-x-auto">
                <div class="grid gap-1 p-2 content-start border rounded">
                    <div class="relative flex">
                        <div class="flex gap-1 grow">
                            <img class="w-5 h-5 rounded-full" src="/images/profile/{{ $post->user->photo ?? 'avatar.webp' }}" />
                            <a class="font-bold" href="{{ route('users.show', $post->user->id) }}/{{ urlencode($post->user->name) }}">{{ $post->user->name }}</a>
                        </div>
                        @if($auth && $auth->id == $post->user->id)
                            <a href="{{ route('status.edit', $post->id) }}" class="float-right text-sm p-1 text-gray-500 bg-blue-200 rounded shadow cursor-pointer hover:bg-blue-300">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" viewBox="0 0 512 512">
                                    <path stroke="currentColor" d="M410.3 231l11.3-11.3-33.9-33.9-62.1-62.1L291.7 89.8l-11.3 11.3-22.6 22.6L58.6 322.9c-10.4 10.4-18 23.3-22.2 37.4L1 480.7c-2.5 8.4-.2 17.5 6.1 23.7s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2L387.7 253.7 410.3 231zM160 399.4l-9.1 22.7c-4 3.1-8.5 5.4-13.3 6.9L59.4 452l23-78.1c1.4-4.9 3.8-9.4 6.9-13.3l22.7-9.1v32c0 8.8 7.2 16 16 16h32zM362.7 18.7L348.3 33.2 325.7 55.8 314.3 67.1l33.9 33.9 62.1 62.1 33.9 33.9 11.3-11.3 22.6-22.6 14.5-14.5c25-25 25-65.5 0-90.5L453.3 18.7c-25-25-65.5-25-90.5 0zm-47.4 168l-144 144c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6l144-144c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z"/>
                                </svg>
                            </a>
                        @endif
                    </div>
                    <div>
                        <p class="break-words">{!! clickable($post->body ?? '') !!}</p>
                        <span class="float-right text-gray-500 text-sm">
                            @php
                                $diff = $post->created_at->shortRelativeDiffForHumans();
                                $diff = str_replace(' ago', '', $diff);
                                $diff = str_replace('mos', 'mon', $diff);
                            @endphp
                            {{ $diff }}
                        </span>
                        @if($post->link)
                            @php
                            $pathtofile = '/posts/'.$post->link;
                            $info = pathinfo($pathtofile);
                            $imagePath = public_path("/posts/$post->link");
                            $imagePath = str_replace('/public/', '/public_html/', $imagePath);
                            if(is_readable($imagePath)) {
                                list($width, $height) = getimagesize($imagePath);
                            }
                            @endphp
                            @if ($info["extension"] == "webp")
                                <a href="{{ "/posts/$post->link" }}" id="link-{{ $post->id }}">
                                    <img src="{{ url("/posts/$post->link") }}" class="mx-auto md:w-auto md:max-h-[30rem] max-w-full" width="{{ $width }}" height="{{ $height }}" />
                                </a>
                            @else
                                <div class="flex content-start text-theme-primary underline">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4" viewBox="0 0 512 512">
                                        <path d="M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zm368 56a24 24 0 1 1 0 48 24 24 0 1 1 0-48z"/>
                                    </svg>
                                    <a href="{{ url("/posts/$post->link") }}" id="link-{{ $post->id }}" download>{{ $info["filename"] }}</a>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="flex bg-gray-100 gap-3 px-2">
                    <span class="flex items-center gap-2">
                        <small class="hover:underline cursor-pointer">{{ $post->likes->count() }}</small>
                        <svg onclick="like({{ $post->id }}, 1)" class="w-4 hover:text-theme-primary cursor-pointer @if(likes($post, 1)) text-theme-primary  @endif" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path stroke="currentColor" @if(likes($post, 1)) fill="currentColor"  @endif d="M3 12.5C3 11.3954 3.89543 10.5 5 10.5H6C7.10457 10.5 8 11.3954 8 12.5V18.5C8 19.6046 7.10457 20.5 6 20.5H5C3.89543 20.5 3 19.6046 3 18.5V12.5Z" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
                            <path stroke="currentColor" @if(likes($post, 1)) fill="currentColor"  @endif d="M8 12.5C9 12.5 13 10 13 5C13 3 16 3 16 5C16 7 16 8 15 10.5H21C21.5523 10.5 22 10.9477 22 11.5V14.7396C22 15.2294 21.8202 15.7022 21.4948 16.0683L18.5967 19.3287C18.2172 19.7557 17.6731 20 17.1019 20H10.3333C10.117 20 9.90643 19.9298 9.73333 19.8L8 18.5" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </span>
                    <span class="text-gray-400">|</span>
                    <span class="flex items-center gap-2">
                        <small class="hover:underline cursor-pointer">{{ $post->dislikes->count() }}</small>
                        <svg onclick="like({{ $post->id }}, 2)" class="w-4 hover:text-theme-primary cursor-pointer @if(likes($post, 2)) text-theme-primary  @endif" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path stroke="currentColor" @if(likes($post, 2)) fill="currentColor"  @endif d="M22 11.5C22 12.6046 21.1046 13.5 20 13.5L19 13.5C17.8954 13.5 17 12.6046 17 11.5L17 5.5C17 4.39543 17.8954 3.5 19 3.5L20 3.5C21.1046 3.5 22 4.39543 22 5.5L22 11.5Z" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
                            <path stroke="currentColor" @if(likes($post, 2)) fill="currentColor"  @endif d="M17 11.5C16 11.5 12 14 12 19C12 21 9 21 9 19C9 17 9 16 10 13.5L4 13.5C3.44772 13.5 3 13.0523 3 12.5L3 9.26039C3 8.77056 3.17976 8.29776 3.50518 7.93166L6.40331 4.67126C6.78285 4.24428 7.32686 3.99998 7.89813 3.99998L14.6667 3.99998C14.883 3.99998 15.0936 4.07016 15.2667 4.19998L17 5.49998" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </span>
                    <span class="text-gray-400">|</span>
                    <span class="cursor-pointer hover:underline text-gray-500" onclick="toggleView({{ $post->id }})">{{ $post->comments->count() }} Comments</span>
                </div>
                <div class="p-2 pt-0 hidden bg-gray-100" id="post-{{ $post->id }}">
                    @foreach($post->comments as $comment)
                    <div class="grid gap-0 p-1 px-2 content-start border rounded shadow">
                        <div class="flex gap-1">
                            <img class="w-4 h-4 rounded-full" src="/images/profile/{{ $comment->user->photo ?? 'avatar.webp' }}" />
                            <a class="font-bold text-sm" href="{{ route('users.show', $comment->user->id) }}/{{ urlencode($comment->user->name) }}">{{ $comment->user->name }}</a>
                        </div>
                        <div class="px-2">
                            {!! clickable($comment->text ?? '') !!}
                            <span class="float-right text-gray-500 text-sm">
                                @php
                                    $diff = str_replace(' from now', '', $comment->created_at->shortRelativeDiffForHumans());
                                    $diff = str_replace(' ago', '', $diff);
                                    $diff = str_replace('mos', 'mon', $diff);
                                @endphp
                                {{ $diff }}
                            </span>
                        </div>
                    </div>
                    @endforeach
                    <form action="{{ route('comments.store') }}" class="border overflow-hidden border-gray-400 rounded-b-xl focus-within:outline outline-1 outline-theme-primary flex gap-0 mb-1 items-start bg-white" method="post">
                        @csrf
                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        <textarea
                            name="text"
                            placeholder="Comment..."
                            rows="1"
                            class="grow resize-none overflow-hidden focus:ring-0 focus:ring-offset-0 border-0 bg-transparent text-sm"
                            id="textarea"
                        ></textarea>
                        <button class="p-2 text-blue-500 hover:text-orange-500 transition-all duration-300" type="submit">
                            <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path fill="currentColor" d="M498.1 5.6c10.1 7 15.4 19.1 13.5 31.2l-64 416c-1.5 9.7-7.4 18.2-16 23s-18.9 5.4-28 1.6L284 427.7l-68.5 74.1c-8.9 9.7-22.9 12.9-35.2 8.1S160 493.2 160 480V396.4c0-4 1.5-7.8 4.2-10.7L331.8 202.8c5.8-6.3 5.6-16-.4-22s-15.7-6.4-22-.7L106 360.8 17.7 316.6C7.1 311.3 .3 300.7 0 288.9s5.9-22.8 16.1-28.7l448-256c10.7-6.1 23.9-5.5 34 1.4z"/>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
    <div>
        {{ $posts->links() }}
    </div>
</div>
@endsection
@section('script')
@include('wall-script')
@endsection