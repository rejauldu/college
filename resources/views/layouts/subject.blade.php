@php
use App\Models\Subject;
$subjects = Subject::all();
@endphp
<h4 class="p-3 bg-theme-primary text-white text-center">বিষয়সমূহ</h4>
<ul id="itemList" class="list w-80">
    @foreach($subjects as $s)
    <li>{{ $s->name }}</li>
    @endforeach
</ul>
<!-- Show More link -->
<button id="moreLink" class="mt-4 text-blue-500 underline">Show More</button>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        const listItems = document.querySelectorAll("#itemList li");
        const moreLink = document.getElementById("moreLink");

        // Initially hide all items after the 10th
        listItems.forEach((item, index) => {
            if (index >= 8) {
                item.classList.add("hidden");
            }
        });

        // Show remaining items when "Show More" link is clicked
        moreLink.addEventListener("click", function() {
            listItems.forEach(item => item.classList.remove("hidden"));
            moreLink.classList.add("hidden"); // Hide the "Show More" link
        });
    });
</script>