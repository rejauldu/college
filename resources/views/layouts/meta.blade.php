@php
$obj = request()->route();
if($obj) {
    $route = $obj->getName();
} else {
    $route = "404";
}

$user_desc = config('app.short_bangla').' আমার অনুপ্রেরণার উৎস।';
$general_desc = config('app.short_bangla').' - একটি জ্ঞানের আলোকবর্তিকা। যেখানে স্বপ্ন হাতছানি দেয়, মায়াবি সবুজ ক্যাম্পাসের প্রতিটি বৃক্ষ রূপকথার গল্প শোনায়। (Hasanpur College)';
if($route == "blog.show") {
    $general_desc = excerpt($post->body, 100);
}
@endphp
<meta charset="utf-8">
<meta name="keywords" content="@if(isset($user)) {{ url('/images/profile/$user->photo') }} @else @if(isset($title)) {{ $title }} @else @yield('title') @endif, {{ config('app.short_bangla') }} @endif">
<meta name="description" content="@if(isset($user)) {{ $user->note ?? $user_desc }} @else {{ $general_desc }} @endif" />
<meta name="author" content="ict4today">
<meta name="theme-color" content="{{ env('theme_color') ?? '#0057a8' }}" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="client-secret" content="{{ config('app.hackerearth_client_secret') }}">

<meta property="og:url"                content="{{ url()->current() }}" />
<meta property="og:type"               content="@if(isset($user)) profile @else article @endif" />
<meta property="og:title"              content="@if(isset($title)) {{ $title }} @else @yield('title') @endif" />
<meta property="og:description"        content="@if(isset($user)) {{ $user->note ?? $user_desc }} @else {{ $general_desc }} @endif" />
<meta property="og:image"              content="@if(isset($user)) {{ url('/images/profile/$user->photo') }} @else {{ url('/images/logo.png') }} @endif" />

<title>@if(isset($title)) {{ $title }} @else @yield('title') @endif</title>

<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">

@php
    if($route == "users.show") {
        $url = url("users/".$user->id.'/'.urlencode($user->name));
    } elseif($route == "attendances.show") {
        $url = url("attendances/".$user->hsc."/".$user->roll.'/'.urlencode($user->name));
    } elseif($route == "notices.show") {
        $url = url("notices/".$post->id.'/'.urlencode($post->title));
    } elseif($route == "blog.show") {
        $url = url("blog/".$post->id.'/'.urlencode($post->title));
    } else {
        $url = url()->current();
    }
@endphp
<link rel="canonical" href="{{ $url ?? '' }}">
@include('layouts.schema')
<!-- Scripts -->
@vite('resources/css/app.scss')
@yield('style')