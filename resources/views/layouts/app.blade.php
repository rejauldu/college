<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.meta')
    </head>
    <body class="font-sans antialiased">
        <div class="grid grid-rows-[auto_auto_1fr_auto] min-h-screen">
            @include('layouts.header')
            @yield('content')
            @include('layouts.footer')
            @include('layouts.scripts')
            @yield('script')
        </div>
    </body>
</html>
