<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.meta')
    </head>
    <body class="font-sans antialiased">
        <div class="grid" id="app" data-phone="123456" >
            @include('layouts.header')
            @yield('content')
        </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        @include('layouts.scripts', ["script" => "wall"])
        @yield('script')
    </body>
</html>
