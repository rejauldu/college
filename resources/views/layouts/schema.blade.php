<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "{{ config('app.name') }}",
  "url": "{{ config('app.url') }}",
  "logo": "{{ config('app.url') }}/images/logo.png",
  "sameAs": [
    "{{ config('app.fb') }}"
  ],
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "{{ config('app.phone') }}",
    "contactType": "Admissions Office",
    "areaServed": "Bangladesh",
    "availableLanguage": ["English", "Bengali"]
  },
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "{{ config('app.address') }}",
    "addressLocality": "Cumilla",
    "addressRegion": "Chattogram",
    "postalCode": "3516",
    "addressCountry": "Bangladesh"
  },
  "foundingDate": "1969",
  "description": "{{ config('app.short_bangla') }} - যেখানে স্বপ্ন হাতছানি দেয়, ঝরে পড়া ফুল আবার নতুন জীবন খুঁজে পায়। যেখানে মায়াবি সবুজ ক্যাম্পাসের প্রতিটি বৃক্ষ রূপকথার গল্প শোনায়।"
}
</script>