<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8895896076224126" crossorigin="anonymous"></script>
@if(isset($script))
    @vite("resources/js/$script/main.js")
@else
    @vite("resources/js/app.js")
@endif
<script>
    window.Laravel = {
        isLoggedIn: {{ Auth::check() ? 'true' : 'false' }},
        user: @json(Auth::user())
    };
</script>