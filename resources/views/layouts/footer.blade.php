<footer class="bg-black mt-3">
    <div class="container mx-auto">
        <div class="grid md:grid-cols-2 xl:grid-cols-4">
            <div class="hidden md:block">
                <h5 class="text-white pt-3 pb-2">উর্ধ্বতন দপ্তরসমূহ</h5>
                <ul class="list list-flush list-black">
                    <li class=""><a class="text-white text-decoration-none" href="https://moedu.gov.bd/"> শিক্ষা মন্ত্রণালয়</a></li>
                    <li class=""><a class=" text-white text-decoration-none" href="https://dshe.gov.bd/"> মাধ্যমিক ও উচ্চশিক্ষা অধিদপ্তর</a></li>
                </ul>
            </div>
            <div class="hidden md:block">
                <h5 class="text-white pt-3 pb-2">গুরুত্বপূর্ণ লিংক</h5>
                <ul class="list list-flush list-black">
                    <li class=""><a class=" text-white text-decoration-none" href="https://comillaboard.portal.gov.bd/"> মাধ্যমিক ও উচ্চমাধ্যমিক শিক্ষা বোর্ড, কুমিল্লা</a></li>
                    <li class=""><a class="text-white text-decoration-none" href="https://www.nu.ac.bd/">জাতীয় বিশ্ববিদ্যালয়</a></li>
                </ul>
            </div>
            <div>
                <h5 class="text-white pt-3 pb-2">গুরুত্বপূর্ণ তথ্য</h5>
                <ul class="list list-flush list-black">
                    <li class="list-group-item bg-transparent text-white px-0"> <i class="fa fa-star"></i>EIIN - {{ config('app.eiin') }}</li>
                    <li class="list-group-item bg-transparent text-white px-0"><a class="text-theme-primary text-decoration-none bg-white px-2 rounded font-bold" href="{{ config('app.fb') }}"><i class="fa fa-facebook"></i> Facebook Group</a></li>
                </ul>
            </div>
            <div>
                <h5 class="text-white pt-3 pb-2">যোগাযোগের ঠিকানা</h5>
                <ul class="list list-flush list-black">
                    <li class="list-group-item bg-transparent text-white px-0"><i class="fa fa-phone"></i> {{ config('app.phone') }}</li>
                    <li class="list-group-item bg-transparent text-white px-0"><i class="fa fa-envelope"></i> ইমেইল: {{ config('app.email') }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container mx-auto bg-black text-center text-gray-500">Copyright © {{ date("Y") }}. All rights reserved by <strong>{{ config('app.name') }}</strong></div>
</footer>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>