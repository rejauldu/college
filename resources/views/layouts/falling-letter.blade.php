<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.meta')
    </head>
    <body class="font-sans antialiased">
        <div class="grid" id="app"></div>
        @include('layouts.scripts', ["script" => "falling-letter"])
        @yield('script')
    </body>
</html>
