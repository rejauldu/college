@extends('layouts.app')
@section('title')
হাসানপুর কলেজ এর শিক্ষার্থীদের হাজিরা
@endsection
@section('content')
<div class="container mx-auto">
    <div>
        <!-- Search Form -->
        <form action="{{ route('attendances.index') }}" class="my-4 flex gap-0">
            <input 
                type="text" 
                name="search" 
                placeholder="রোল লিখে সার্চ করুন" 
                value="{{ request('search') }}" 
                class="border border-gray-300 p-2 pl-10 rounded-s-lg rounded-r-none w-full focus:outline-none focus:ring-blue-500"
            >
            <button class="pl-3 rounded-s-none rounded-r-lg flex items-center border px-3 bg-gray-100 text-theme-primary" type="submit">
                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="transparent" aria-hidden="true" viewBox="0 0 24 24" role="img"><path vector-effect="non-scaling-stroke" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" d="M10.688 18.377a7.688 7.688 0 100-15.377 7.688 7.688 0 000 15.377zm5.428-2.261L21 21"></path></svg>
            </button>
        </form>
    </div>
    @if(session()->has('message'))
    <div class="bg-red-300 border border-red-500 text-red-500 rounded px-4 py-2">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="grid gap-2">
        @foreach($users as $user)
        <a class="relative grid grid-cols-[auto_1fr] gap-2 md:gap-4 items-center hover:no-underline odd:bg-white even:bg-blue-50 hover:bg-blue-100" href="{{ route('attendances.show', [$user->hsc, $user->roll]) }}/{{ urlencode($user->name) }}">
            <div class="rounded w-20 h-20 sm:w-28 sm:h-28 md:w-40 md:h-40 border overflow-hidden relative z-10">
                <img class="w-20 h-20 sm:w-28 sm:h-28 md:w-40 md:h-40 object-cover object-center" src="/images/profile/{{ $user->photo ?? 'avatar.webp' }}" />
            </div>
            <div>
                <div class="relative z-10">
                    <h5>{{ $user->name ?? '' }}</h5>
                    <p>{{ year($user) }}</p>
                    <p>রোল: {{ e2b($user->roll) }}</p>
                    <p>গ্রুপ: {{ group($user->roll) }}</p>
                </div>
            </div>
            <!--<div class="absolute inset-0 flex items-center justify-center">-->
            <!--    <span class="text-gray-200 text-3xl font-bold opacity-20">{{ $user->name ?? ''}} এর হাজিরা</span>-->
            <!--</div>-->
        </a>
        @endforeach
    </div>
    <!-- Display pagination links -->
    <div class="mt-4">
        {{ $users->links() }}
    </div>
</div>
@endsection
@section('script')
<script>
    document.querySelectorAll('tr.cursor-pointer').forEach(function(row) {
        row.addEventListener('click', function() {
            window.location.href = row.dataset.href;
        });
    });
</script>
@endsection