@extends('layouts.backend')
@section('title')
{{ __('All Users') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="nav-icon fas fa-th mr-1"></i> {{ __('From Date') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="mb-3 p-5 bg-primary text-white rounded">
                            @foreach($dates as $k=>$v)
                                @if($k == 'dateFrom1')
                                <h2>2nd Year: {{ \Carbon\Carbon::parse($v)->format('d/m/Y')}}</h2>
                                @else
                                <h2>1st Year: {{ \Carbon\Carbon::parse($v)->format('d/m/Y')}}</h2>
                                @endif
                            @endforeach
                        </div>
                        <form action="{{ route('date-from') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="batch">{{ __('HSC') }}</label>
                                <select name="batch" class="form-control mb-3">
                                    <option value="">--Select Batch--</option>
                                    @foreach($batches as $b)
                                    <option value="{{ $b }}">{{ $b }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date">From Date:</label>
                                <input type="text" name="date_from" class="form-control" value="{{ $date ?? '' }}" id="datepicker">
                            </div>
                            <div class="flex items-center gap-4">
                                <button class="btn btn-primary" type="submit">{{ __('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.14.0/themes/base/jquery-ui.css">
@endsection
@section('script')
<script>
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
@endsection