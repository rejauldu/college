@extends('layouts.app')
@section('title')
{{ $user->name }} এর হাজিরা
@endsection
@section('content')
<div class="container mx-auto">
    <h1 class="text-lg md:text-3xl text-center mb-4 md:mb-6 mt-4">{{ $user->name }} এর হাজিরা</h1>
    <div class="overflow-x-auto">
        <table class="table">
            @php
            $i = 0;
            $class = "";
            @endphp
            @foreach($posts as $post)
            @php
            $class="bg-red-100";
            @endphp
            @if($i%2 == 0)
            @php
            $class="bg-red-50";
            @endphp
            @endif
                <tr class="">
                    <td class="flex justify-center @if($post->date>=$date_from) {{ $class }} @endif">
                        @if($post->roll == 0)
                            <span class="text-lg text-red-500" title="Cross">X</span>
                        @else
                            <span title="Tick">
                                <svg class="w-5 text-green-500" viewBox="0 0 90 80" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="20,50 40,70 80,30" fill="none" stroke="currentColor" stroke-width="10" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </span>
                        @endif
                    </td>
                    <td class="@if($post->date>=$date_from) {{ $class }} @endif">
                        {{ $post->date->format("jS M Y") }}
                    </td>
                </tr>
            @php
            $i++;
            @endphp
            @endforeach
        </table>
    </div>
    <div class="mt-6">
        {{ $posts->links() }}
    </div>
</div>
@endsection
@section('style')
<style>
    .bg-red-100 {
        background:#FFEEED;
    }
</style>
@endsection