<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.meta', ['title' => "Page not found"])
    </head>
    <body class="font-sans antialiased">
        <div class="grid grid-rows-[auto_auto_1fr_auto] min-h-screen">
            @include('layouts.header')
            <div class="container mx-auto text-center py-5">
                <h1 class="text-red-500">404</h1>
                <p class="text-xl">Oops! The page you're looking for can't be found.</p>
                <p>It might have been removed, renamed, or didn't exist in the first place.</p>
                <a href="/" class="underline text-theme-primary">Go Back to Homepage</a>
            </div>
            @include('layouts.footer')
            @yield('script')
        </div>
    </body>
</html>