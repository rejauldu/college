<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.meta')
    </head>
    <body class="font-sans antialiased">
        <div class="grid grid-rows-[auto_auto_1fr_auto] min-h-screen">
            @include('layouts.header')
            <div class="container mx-auto text-center py-5">
                <h1>410</h1>
                <p class="text-xl">Sorry! The page you’re looking for has been permanently removed.</p>
                <p>It may no longer be available on our site.</p>
                <a href="/" class="underline text-theme-primary">Go Back to Homepage</a>
            </div>
            @include('layouts.footer')
            @yield('script')
        </div>
    </body>
</html>