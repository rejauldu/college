@extends('layouts.app')
@section('title')
{{ config('app.name') }} | {{ config('app.short_bangla') }}
@endsection
@section('content')
@include('home.masthead')
@include('home.notices')
@include('notices.notice-cats')
@include('home.vacations')
@endsection