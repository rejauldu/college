@extends('layouts.app')
@section('title')
Academic Rules of {{ config('app.name') }}
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid gap-3">
        <h1 class="text-3xl text-center">{{ $post->title ?? '' }}</h1>
        <div class="grid gap-3">{!! $post->body ?? '' !!}</div>
    </div>
</div>
@endsection