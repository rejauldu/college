<div class="container mx-auto my-5">
    <div class="grid sm:grid-cols-2 lg:grid-cols-4 gap-3 border p-4 rounded-xl bg-gray-100">
        <div class="border border-theme-primary rounded">
            <h4 class="bg-theme-primary text-white px-2 capitalize">HSC Admission</h4>
            <div class="px-2">
                @foreach($hsc_admission as $ha)
                <p><a href="{{ route('notices.show', $ha->id) }}/{{ urlencode($ha->title) }}" class="text-sm">{{ excerpt($ha->title, 14) }}</a></p>
                <div class="border"></div>
                @endforeach
            </div>
        </div>
        <div class="border border-theme-primary rounded">
            <h4 class="bg-theme-primary text-white px-2 capitalize">HSC Routine</h4>
            <div class="px-2">
                @foreach($hsc_routine as $hr)
                <p><a href="{{ route('notices.show', $hr->id) }}/{{ urlencode($hr->title) }}" class="text-sm">{{ excerpt($hr->title, 14) }}</a></p>
                <div class="border"></div>
                @endforeach
            </div>
        </div>
        <div class="border border-theme-primary rounded">
            <h4 class="bg-theme-primary text-white px-2 capitalize">Degree Admission</h4>
            <div class="px-2">
                @foreach($degree_admission as $da)
                <p><a href="{{ route('notices.show', $da->id) }}/{{ urlencode($da->title) }}" class="text-sm">{{ excerpt($da->title, 14) }}</a></p>
                <div class="border"></div>
                @endforeach
            </div>
        </div>
        <div class="border border-theme-primary rounded">
            <h4 class="bg-theme-primary text-white px-2 capitalize">Degree Routine</h4>
            <div class="px-2">
                @foreach($degree_routine as $dr)
                <p><a href="{{ route('notices.show', $dr->id) }}/{{ urlencode($dr->title) }}" class="text-sm">{{ excerpt($dr->title, 14) }}</a></p>
                <div class="border"></div>
                @endforeach
            </div>
        </div>
    </div>
</div>