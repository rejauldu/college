@extends('layouts.backend')
@section('title')
{{ __(isset($post)?'Update post':'Create post') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($post)?'Update Notice':'Add Notice') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                                <form action="@if(isset($post)) {{ route('notices.update', $post->id) }} @else {{ route('notices.store') }} @endif" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($post))
                                    @method('PUT')
                                    @endif
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input id="title" type="text" class="form-control" name="title" value="{{ $post->title ?? '' }}" placeholder="Tittle" title="Enter title" />
                                    </div>
                                    <div class="form-group">
                                        <label for="body">Body:</label>
                                        <textarea name="body" class="form-control editor-tools" rows="5" id="body">{{ $post->body ?? '' }}</textarea>
                                        <div class="valid-feedback">Valid.</div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <label for="files">Image/PDF file</label>
                                            <input type="file" onchange="uploadFiles()" id="files" name="files" class="form-control bg-theme text-white" accept="application/pdf,image/*" value="Upload image" multiple/>
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                        <div class="col-6 form-group">
                                            <a href="{{ asset('/posts') }}/{{ $post->link ?? 'not-found.webp' }}" class="btn btn-link fa fa-download" download> {{ $post->link ?? '' }}</a>
                                        </div>
                                        <div class="col-12">
                                            <!-- Bootstrap Progress Bar -->
                                            <div class="progress mt-3 rounded-lg" style="height: 25px;">
                                                <div id="progressBar" class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div id="imagePreview" class="d-flex flex-wrap mt-3 bg-black rounded-lg">
                                                @if(isset($post))
                                                @foreach($post->images as $image)
                                                <img src='{{ url("/posts/uploads/$image->name") }}' class="img-thumbnail border border-white m-2" style="width:100px" />
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="is_moving" value="1" @if(isset($post) && $post->metas->count() && $post->metas[0]->meta_value == 1) checked @endif>Moving Banner
                                            </label>
                                        </div>
                                    </div>
                                    @php
                                    $notice_cat = null;
                                    if(isset($post) && $post->metas->count()>1) {
                                        $notice_cat = $post->metas[1]->meta_value;
                                    }
                                    @endphp
                                    <div class="col-12">
                                        <select name="notice_cat" class="custom-select my-3">
                                            <option value="">Select Category</option>
                                            <option value="hsc-admission" @if($notice_cat == "hsc-admission") selected @endif>HSC Admission</option>
                                            <option value="hsc-routine" @if($notice_cat == "hsc-routine") selected @endif>HSC exam Routine</option>
                                            <option value="degree-admission" @if($notice_cat == "degree-admission") selected @endif>Deg. Admission</option>
                                            <option value="degree-routine" @if($notice_cat == "degree-routine") selected @endif>Deg. exam Routine</option>
                                        </select>
                                    <div>
                                    <div class="form-group mt-3">
                                        <button class="btn btn-success btn-theme" type="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
function uploadFiles() {
    // Reset progress bar and status text
    let progressBar = document.getElementById('progressBar');
    progressBar.style.width = '0%';
    progressBar.innerHTML = '0%';
    progressBar.classList.remove('bg-success', 'bg-danger');

    // Clear previous images from the preview area
    document.getElementById('imagePreview').innerHTML = '';

    // Get the files from the input
    let files = document.getElementById('files').files;

    // Check if files are selected
    if (files.length === 0) {
        alert("No files selected!");
        return;
    }

    // Prepare FormData
    let formData = new FormData();
    for (let i = 0; i < files.length; i++) {
        formData.append('files[]', files[i]);
    }

    // AJAX request
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '{{ route("images.store") }}', true); // Laravel route for storing images
    xhr.setRequestHeader('X-CSRF-TOKEN', document.querySelector('meta[name="csrf-token"]').getAttribute('content'));

    // Event listener for upload progress
    xhr.upload.addEventListener('progress', function(e) {
        if (e.lengthComputable) {
            let percentComplete = (e.loaded / e.total) * 100;
            progressBar.style.width = percentComplete + '%';
            progressBar.setAttribute('aria-valuenow', percentComplete);
            progressBar.innerHTML = Math.round(percentComplete) + '%';
        }
    });

    // Event listener for response
    xhr.onload = function() {
        if (xhr.status === 200) {
            let response = JSON.parse(xhr.responseText);
            progressBar.classList.add("bg-success");

            // Display uploaded images below the input
            if (response.success) {
                let imagePreview = document.getElementById('imagePreview');
                response.names.forEach(function(name) {
                    let img = document.createElement('img');
                    img.src = `/posts/uploads/${name}`;
                    img.alt = 'Uploaded Image';
                    img.classList.add('img-thumbnail', 'border', 'border-white', 'm-2');
                    img.style.width = '100px';
                    imagePreview.appendChild(img);
                });
            }
        } else {
            progressBar.classList.add("bg-danger");
            alert("Error: " + xhr.statusText);
        }
    };

    xhr.onerror = function() {
        progressBar.classList.add("bg-danger");
        alert("An error occurred during the upload.");
    };

    xhr.send(formData);
}
</script>
@endsection