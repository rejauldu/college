@extends('layouts.app')

@section('title')
    {{ $post->title ?? '' }}
@endsection

@section('content')
<div class="container mx-auto min-h-screen">
    <div class="grid lg:grid-cols-[auto_1fr_auto] py-3">
        <div class="hidden lg:block">
            @include('layouts.info')
        </div>
        
        <div class="lg:px-3">
            <h1 class="text-3xl capitalize">{{ $post->title ?? '' }}</h1>
            <div class="mb-3">
                <p class="text-gray-500 text-sm">Posted at: {{ $post->created_at->format('jS M Y') }}</p>
            </div>
            <div class="mb-3">
                {!! $post->body ?? '' !!}
            </div>
            
            <!-- Display all related images with modal trigger -->
            <div class="grid grid-cols-1 @if($post->images->count() == 2) md:grid-cols-2 @elseif($post->images->count() > 2) md:grid-cols-3 @endif gap-4">
                @foreach ($post->images as $image)
                    @php
                        $pathtofile = '/posts/uploads/' . $image->name;
                        $info = pathinfo($pathtofile);
                        $extension = $info['extension'] ?? null;
                    @endphp
                    
                    @if ($extension === "pdf")
                        <!-- Display PDF file -->
                        <object data="{{ $pathtofile }}" type="application/pdf" width="600" height="500" class="w-full overflow-hidden border-4">
                            <p class="bg-theme-light p-3 rounded">
                                <a class="underline" href="{{ $pathtofile }}" download>
                                    {{ $post->title ?? 'Download PDF' }}
                                </a>
                            </p>
                            <embed src="{{ $pathtofile }}" width="100%" height="600px"/>
                        </object>
                    @elseif ($extension)
                        <!-- Clickable image that triggers the modal -->
                        <img 
                            class="w-full border p-1 cursor-pointer" 
                            src="{{ $pathtofile }}" 
                            alt="{{ $post->title ?? '' }}" 
                            onclick="openModal('{{ $pathtofile }}')" 
                        />
                    @endif
                @endforeach
            </div>
        </div>
        
        <div class="hidden lg:block">
            @include('layouts.subject')
        </div>
    </div>
</div>

<!-- Modal for image display -->
<div id="imageModal" class="fixed inset-0 bg-black bg-opacity-70 flex items-center justify-center hidden">
    <div class="relative w-full max-w-3xl mx-auto">
        <img id="modalImage" src="" alt="" class="w-full rounded-lg shadow-lg">
        <button 
            onclick="closeModal()" 
            class="absolute top-2 right-2 bg-white rounded-full p-1 text-black hover:bg-gray-200">
            &times;
        </button>
    </div>
</div>

<script>
    function openModal(imageSrc) {
        document.getElementById('modalImage').src = imageSrc;
        document.getElementById('imageModal').classList.remove('hidden');
    }

    function closeModal() {
        document.getElementById('imageModal').classList.add('hidden');
    }
</script>
@endsection