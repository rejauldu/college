@extends('layouts.noFooter')
@section('title')
Edit
@endsection
@section('content')
<div class="container mx-auto">
    @if(session()->has('message'))
    <div class="p-2 my-2 text-blue-500 border rounded">
        {{ session()->get('message') }}
    </div>
    @endif
    <form action="{{ route('status.update', $post->id) }}" method="post" class="border overflow-hidden border-gray-400 rounded mt-4 focus-within:outline outline-1 outline-theme-primary flex gap-0 mb-1 items-start bg-gray-100" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="relative inline-block py-1 bg-blue-200" id="file">
            <div class="flex items-center justify-center p-2 cursor-pointer">
                <svg class="w-6 h-6 text-blue-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M12 2l5.5 5.5h-3v4h-5v-4h-3L12 2z" />
                    <path d="M12 15.5c-1.4 0-2.6-.6-3.5-1.5l-1.4 1.4c1.3 1.4 3 2.1 4.9 2.1 1.9 0 3.6-.7 4.9-2.1l-1.4-1.4c-.9.9-2.1 1.5-3.5 1.5z" />
                    <path d="M3 13v4h18v-4H3z" />
                </svg>
            </div>
            <input type="file" name="link" class="absolute inset-0 w-full h-full opacity-0 cursor-pointer" onchange="preview(event)" />
        </div>
        <textarea
            name="body"
            placeholder="Write..."
            rows="1"
            class="grow resize-none overflow-hidden focus:ring-0 focus:ring-offset-0 border-0 relative bg-transparent"
            id="textarea"
        >{{ $post->body ?? "" }}</textarea>
        <button class="p-2 text-blue-500 hover:text-orange-500 transition-all duration-300" type="submit">
            <svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path fill="currentColor" d="M498.1 5.6c10.1 7 15.4 19.1 13.5 31.2l-64 416c-1.5 9.7-7.4 18.2-16 23s-18.9 5.4-28 1.6L284 427.7l-68.5 74.1c-8.9 9.7-22.9 12.9-35.2 8.1S160 493.2 160 480V396.4c0-4 1.5-7.8 4.2-10.7L331.8 202.8c5.8-6.3 5.6-16-.4-22s-15.7-6.4-22-.7L106 360.8 17.7 316.6C7.1 311.3 .3 300.7 0 288.9s5.9-22.8 16.1-28.7l448-256c10.7-6.1 23.9-5.5 34 1.4z"/>
            </svg>
        </button>
    </form>
    <div id="preview">
        @if($post->link)
            @php
            $pathtofile = '/posts/'.$post->link;
            $info = pathinfo($pathtofile);
            @endphp
            @if ($info["extension"] == "webp")
                <a href="{{ url("/posts/$post->link") }}" id="link-{{ $post->id }}"><img src="{{ url("/posts/$post->link") }}" class="w-20 h-20 border rounded shadow border-gray-500" /></a>
            @else
                <div class="flex content-start text-theme-primary underline">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4" viewBox="0 0 512 512">
                        <path d="M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zm368 56a24 24 0 1 1 0 48 24 24 0 1 1 0-48z"/>
                    </svg>
                    <a href="{{ url("/posts/$post->link") }}" id="link-{{ $post->id }}" download>{{ $info["filename"] }}</a>
                </div>
            @endif
        @endif
    </div>
</div>
@endsection
@section('script')
@include('wall-script')
@endsection