@extends('layouts.app')
@section('title')
Contact {{ config('app.address') }}
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid gap-1 md:gap-5 mb-3 md:mb-5 border-b bg-theme-light">
        <img
            src="/posts/map.webp"
            width="1600"
            height="689"
            alt="Contact {{ config('app.name') }}"
            class="border p-1 rounded"
        />
    </div>
    <div class="grid gap-3 p-3 md:p-5 text-justify bg-[#135] border text-white text-lg rounded">
        <p>Address: {{ config('app.address') }}</p>
        <p>Phone: <a href="callto:{{ config('app.phone') }}">{{ config('app.phone') }}</a></p>
        <p>Email: <a href="mailto:{{ config('app.email') }}">{{ config('app.email') }}</a></p>
    </div>
</div>
@endsection