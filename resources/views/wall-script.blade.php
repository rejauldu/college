<script>
    const textarea = document.getElementById('textarea');
    textarea?.addEventListener('input', function() {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight + 'px';
    });
    textarea?.addEventListener("keydown", function(e) {
        if(e.keyCode === 13) {
            this.style.height = this.scrollHeight + 'px';
        }
    });
    function toggleView(post) {
        const e = document.getElementById("post-"+post);
        e.classList.toggle("hidden");
    }
    function preview(event, file=null) {
        const preview = document.getElementById('preview');
        preview.innerHTML = ''; // Clear any previous content

        if (!file) {
            file = event.target.files[0];
        }
        
        if (file) {
            if (file.type?.startsWith('image/')) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    const img = document.createElement('img');
                    img.src = e.target.result;
                    img.classList.add('w-20');
                    img.classList.add('h-20');
                    img.classList.add('border');
                    img.classList.add('rounded');
                    img.classList.add('shadow');
                    img.classList.add('border-gray-500');
                    img.classList.add('mb-2');
                    preview.appendChild(img);
                };
                reader.readAsDataURL(file);
            } else {
                const fileName = document.createElement('p');
                fileName.textContent = `Selected file: ${file.name || file}`;
                fileName.classList.add('border');
                fileName.classList.add('rounded');
                fileName.classList.add('shadow');
                fileName.classList.add('border-gray-500');
                fileName.classList.add('mb-2');
                fileName.classList.add('p-2');
                preview.appendChild(fileName);
            }
        }
    }
    function like(post, react) {
        @guest
        window.location = "{{ route('login') }}"
        @endguest
        const url = "{{ route('likes.store') }}";
        
        const data = {
            post_id: post,
            react: react,
            _token: "{{ csrf_token() }}"
        };
        
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            location.reload();
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error); // Handle errors
        });
    }
</script>