@extends('layouts.app')
@section('title')
About {{ config('app.name') }}
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid gap-1 md:gap-5 mb-3 md:mb-5 border-b bg-theme-light">
        <img
            src="/posts/{{ $post->link }}"
            width="1600"
            height="689"
            alt="About {{ config('app.name') }}"
            class="border p-1 rounded"
        />
    </div>
    <div class="grid gap-3 mb-5 text-justify">{!! $post->body !!}</div>
</div>
@endsection