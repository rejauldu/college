@extends('layouts.app')
@section('title')
Principal's Message
@endsection
@section('content')
<div class="container mx-auto">
    <div class="grid grid-cols-[auto_1fr] gap-1 md:gap-5 mb-3 md:mb-5 border-b p-3 md:p-5 bg-theme-light">
        <img
            src="/posts/{{ $post->link }}"
            width="215"
            height="270"
            alt="{{ $post->title }}"
            class="border p-1 rounded max-w-[15rem]"
        />
        <div class="grid gap-1 content-center">
            <h1>{{ $post->title }}</h1>
            <h4 class="text-lg">Principal</h4>
            <p class="text-gray-600">Hasanpur Shahid Nazrul Government College</p>
        </div>
    </div>
    <div class="grid gap-3 mb-5 text-justify">{!! $post->body !!}</div>
    <p>Warm Regards,</p>
    <p class="text-xl">{{ $post->title }}</p>
    <h4 class="text-lg">Principal</h4>
    <p class="text-gray-500 ">Hasanpur Shahid Nazrul Government College</p>
</div>
@endsection