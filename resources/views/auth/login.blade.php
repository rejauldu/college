@extends('layouts.app')
@section('title')
Login | লগইন
@endsection
@section('content')
<div class="container mx-auto">
    <div class="sm:min-h-screen grid sm:justify-center content-center">
        <div class="px-4 py-10 sm:min-w-[24rem] border rounded">
            <div class="text-xl text-center bg-gray-200 p-2 rounded mb-6">{{ __('Login') }}</div>
            <form class="" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="mb-6">
                    <label for="phone" class="md:text-right">{{ __('Phone') }}</label>
                    <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-6">
                    <label for="password" class="md:text-righ">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-6">
                    <input class="radio-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="radio-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>

                <div class="mb-0 flex items-start flex-wrap">
                    <button type="submit" class="button-primary mb-10" onClick="this.form.submit(); this.disabled=true; this.value='Logging in…'; ">
                        {{ __('Login') }}
                    </button>
                    <a href="{{ route('facebook.login') }}" class="ml-auto py-2 px-4 flex justify-center items-center bg-blue-600 hover:bg-blue-700 hover:no-underline focus:ring-blue-500 focus:ring-offset-blue-200 text-white transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2 rounded-lg">
                        <svg width="20" height="20" fill="currentColor" class="mr-2" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"></path>
                        </svg>
                        Sign in with Facebook
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection