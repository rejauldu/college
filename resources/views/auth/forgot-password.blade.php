@extends('layouts.app')
@section('content')
<div class="container mx-auto">
    <div class="sm:min-h-screen grid sm:justify-center content-center">
        <div class="px-4 py-10 sm:min-w-[24rem] border rounded">
            <div class="text-xl text-center bg-gray-200 p-2 rounded mb-6">{{ __('Forgot Password') }}</div>
        </div>
    </div>
</div>
@endsection