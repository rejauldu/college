@extends('layouts.app')
@section('title')
About {{ config('app.name') }}
@endsection
@section('content')
<div class="container mx-auto">
    <h1>User Facebook Stories</h1>
    @if(isset($stories['data']) && count($stories['data']))
        <ul>
            @foreach($stories['data'] as $story)
                <li>
                    <p><strong>Story ID:</strong> {{ $story['id'] }}</p>
                    <p><strong>Message:</strong> {{ $story['message'] ?? 'No message' }}</p>
                    @if(isset($story['attachments']))
                        <img src="{{ $story['attachments']['data'][0]['media']['image']['src'] }}" alt="Story Image" style="max-width: 300px;">
                    @endif
                </li>
            @endforeach
        </ul>
    @else
        <p>No stories found.</p>
    @endif
</div>
@endsection