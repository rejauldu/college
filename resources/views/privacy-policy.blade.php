@extends('layouts.app')
@section('title')
About {{ config('app.name') }}
@endsection
@section('content')
<div class="container mx-auto">
    <div class="container mx-auto p-8">
        <header class="text-center mb-6">
            <h1 class="text-4xl font-bold text-green-600">Privacy Policy</h1>
            <p class="text-sm text-gray-600">Last Updated: January 2, 2025</p>
        </header>

        <section class="bg-white p-6 rounded-lg shadow-md">
            <p class="mb-4">
                Welcome to the official website of <strong>Hasanpur Shaheed Nazrul Govt. College</strong> 
                (<a href="https://hasanpursncollege.edu.bd" class="text-blue-500 hover:underline">hasanpursncollege.edu.bd</a>). 
                We value your privacy and are committed to protecting your personal information. This privacy policy explains how we collect, use, and protect your data.
            </p>
        </section>

        <section class="bg-white p-6 rounded-lg shadow-md mt-6">
            <h2 class="text-2xl font-semibold text-green-600 mb-4">1. Information We Collect</h2>
            <div class="mb-4">
                <h3 class="text-lg font-semibold">a. Automatically Collected Information</h3>
                <ul class="list-disc list-inside">
                    <li><strong>Log Data:</strong> We collect details such as your IP address, browser type, device type, and pages you visit on the website.</li>
                    <li><strong>Cookies:</strong> Cookies are used to enhance your browsing experience by remembering user preferences and website activity.</li>
                </ul>
            </div>
            <div>
                <h3 class="text-lg font-semibold">b. Information You Provide</h3>
                <ul class="list-disc list-inside">
                    <li><strong>Contact Forms:</strong> When you submit inquiries or requests, we may collect details like your name, email, phone number, etc.</li>
                    <li><strong>Online Applications:</strong> For admissions or academic purposes, we may collect personal and academic information.</li>
                </ul>
            </div>
        </section>

        <section class="bg-white p-6 rounded-lg shadow-md mt-6">
            <h2 class="text-2xl font-semibold text-green-600 mb-4">2. How We Use Your Information</h2>
            <p class="mb-4">We use the collected information for the following purposes:</p>
            <ul class="list-disc list-inside">
                <li>To improve website functionality and user experience.</li>
                <li>To respond to inquiries or requests submitted via contact forms.</li>
                <li>To process admissions and provide academic services.</li>
            </ul>
        </section>

        <section class="bg-white p-6 rounded-lg shadow-md mt-6">
            <h2 class="text-2xl font-semibold text-green-600 mb-4">3. How We Protect Your Information</h2>
            <p>We take the security of your data seriously and implement measures to prevent unauthorized access, use, or disclosure. These include:</p>
            <ul class="list-disc list-inside">
                <li>Using secure servers and encrypted data transmission.</li>
                <li>Regularly updating our systems to address potential vulnerabilities.</li>
            </ul>
        </section>

        <section class="bg-white p-6 rounded-lg shadow-md mt-6">
            <h2 class="text-2xl font-semibold text-green-600 mb-4">4. Third-Party Links</h2>
            <p>Our website may include links to third-party websites. Please note that we are not responsible for their privacy practices or content. We recommend reviewing their privacy policies.</p>
        </section>

        <section class="bg-white p-6 rounded-lg shadow-md mt-6">
            <h2 class="text-2xl font-semibold text-green-600 mb-4">5. Changes to This Privacy Policy</h2>
            <p>We may update this privacy policy periodically. Any changes will be posted on this page, and the "Last Updated" date will be revised accordingly.</p>
        </section>

        <footer class="text-center mt-8">
            <p class="text-gray-600 text-sm">&copy; 2025 Hasanpur Shaheed Nazrul Govt. College. All rights reserved.</p>
        </footer>
    </div>
</div>
@endsection