@extends('layouts.app')
@section('title')
Blog Post | ব্লগ পোস্ট
@endsection
@section('content')
<div class="container mx-auto g">
    <div class="grid lg:grid-cols-[auto_1fr_auto]">
        <div class="hidden lg:block"></div>
        <div class="lg:px-3 grid gap-4">
            @foreach($posts as $post)
            <div class="grid gap-2 grid-cols-[auto_1fr]">
                <div class=""><img src="{{ url('/') }}/image/80/80/{{ $post->link ?? 'default.webp' }}" alt="{{ $post->title ?? '' }}" class="w-20 h-20" /></div>
                <div class="">
                    <h4><a href="{{ route('blog.show', $post->id) }}/{{ urlencode($post->title) }}">{{ $post->title }}</a></h4>
                    <hr/>
                    <span class="text-sm text-gray-400">Posted at {{ $post->created_at->format('jS M Y') }}</span>
                    <hr/>
                    <div class="text-justify">{!! excerpt($post->body, 30) !!} <a class="btn btn-link text-theme-primary" href="{{ route('blog.show', $post->id) }}/{{ urlencode($post->title) }}">...more</a></div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="hidden lg:block"></div>
    </div>
    <div>
        {{ $posts->links() }}
    </div>
</div>
@endsection