@extends('layouts.backend')
@section('title')
{{ __(isset($post)?'Update post':'Create post') }}
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        @if(session()->has('message'))
        <div class="alert alert-warning">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card card-light">
                    <div class="card-header with-border">
                        <h3 class="card-title"><i class="fa fa-edit"></i> {{ __(isset($post)?'Update Notice':'Add Notice') }}</h3>
                        <div class="card-tools float-right">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row pt-2">
                            <div class="col-12"><!--left col-->
                                <form action="@if(isset($post)) {{ route('blog.update', $post->id) }} @else {{ route('blog.store') }} @endif" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(isset($post))
                                    @method('PUT')
                                    @endif
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input id="title" type="text" class="form-control" name="title" value="{{ $post->title ?? '' }}" placeholder="Tittle" title="Enter title" />
                                    </div>
                                    <div class="form-group">
                                        <label for="body">Body:</label>
                                        <textarea name="body" class="form-control editor-tools" rows="5" id="body">{{ $post->body ?? '' }}</textarea>
                                        <div class="valid-feedback">Valid.</div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <label for="link">Image/PDF file</label>
                                            <input type="file" id="link" name="link" class="form-control bg-theme text-white" accept="application/pdf,image/*" value="Upload image" />
                                            <div class="valid-feedback">Valid.</div>
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>
                                        <div class="col-6 form-group">
                                            <a href="{{ asset('/posts') }}/{{ $post->link ?? 'not-found.webp' }}" class="btn btn-link fa fa-download" download> {{ $post->link ?? '' }}</a>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <button class="btn btn-success btn-theme" type="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection