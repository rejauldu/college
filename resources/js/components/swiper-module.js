import Swiper from 'swiper';
import { Autoplay, Navigation, Zoom } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/zoom';

/**
 * Swiper js module customizations class
 */
class SwiperModule {

    // eslint-disable-next-line require-jsdoc
    constructor() {
        this.normalSlider = document.querySelector( '.normal-slider' );
        this.routineSlider = document.querySelector('.routine-slider');
    }

    /**
     * Attaches event listeners to the hamburger icon and close menu icon to toggle the mobile navigation menu.
     * @function events
     * @memberof [Name of the class or component where this function is defined]
     * @returns {void}
     */
    events() {
        this.initiateNormalSlider();
        this.initiateRoutineSlider();
    }

    /**
     * Initializes a normal slider using the Swiper library.
     *
     * @returns {void}
     */
    initiateNormalSlider() {
        if (!this.normalSlider) return;

        Swiper.use([Navigation, Autoplay]);

        new Swiper(this.normalSlider, {
            slidesPerView: 'auto',
            loop:true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false
            },
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });
    }
    /**
     * Initializes a normal slider using the Swiper library.
     *
     * @returns {void}
     */
    initiateRoutineSlider() {
        if ( !this.routineSlider ) return;
        const weekday = new Date().getDay();
        const hour = new Date().getHours();
        let slide = hour > 14 ? weekday + 1 : weekday;
        slide = slide > 4 ? 0 : slide;
        Swiper.use([Navigation, Autoplay, Zoom]);

        new Swiper(this.routineSlider, {
            slidesPerView: 'auto',
            initialSlide: slide,
            autoplay: false,
            zoom: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });
    }
}

export default SwiperModule;