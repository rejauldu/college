import './bootstrap';
import Navbar from './components/navbar';
import SwiperModule from './components/swiper-module';
import Accordion from './components/accordion';

new Navbar();
new SwiperModule().events();
new Accordion();
