import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig( {
    plugins: [
        laravel({
            input: [
                'resources/css/app.scss',
                'resources/js/app.js',
                'resources/js/c/main.js',
                'resources/js/falling-letter/main.js',
                'resources/js/profile/main.js',
                'resources/js/wall/main.js',
            ],
            refresh: true,
        }),
        vue(),
    ]
});
