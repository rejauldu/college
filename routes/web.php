<?php

use App\Http\Controllers\AccessoriesController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DateController;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\HomeSliderController;
use App\Http\Controllers\HuggingFaceController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\RoutineController;
use App\Http\Controllers\SyncController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/',[AdminController::class, 'index'])->name('admin');
    
    Route::get('/blog/list', [BlogController::class, 'list'])->name('blog.list');
    Route::resource('blog', BlogController::class)->except(['index', 'show']);
    
    Route::resource('dates', DateController::class);
    
    Route::get('/date-from',[AttendanceController::class, 'dateFrom'])->name('date-from');
    Route::post('/date-from',[AttendanceController::class, 'dateFromStore']);

    Route::get('/cache', [AccessoriesController::class, 'cache']);
    Route::get('/clear', [AccessoriesController::class, 'clearCache']);

    Route::get('/home-sliders/list', [HomeSliderController::class, 'list'])->name('home-sliders.list');
    Route::resource('home-sliders', HomeSliderController::class);
    Route::resource('images', ImageController::class);
    Route::resource('notices', NoticeController::class)->except(['show']);
    Route::get('/notices/list', [NoticeController::class, 'list'])->name('notices.list');

    Route::get('/results/export', [ResultController::class, 'export'])->name('results.export');
    Route::post('/results/download', [ResultController::class, 'download'])->name('results.download');
    Route::get('/results/import', [ResultController::class, 'import'])->name('results.import');
    Route::get('/results/list', [ResultController::class, 'list'])->name('results.list');
    Route::post('/results/upload', [ResultController::class, 'upload'])->name('results.upload');
    Route::resource('results', ResultController::class)->except(['index']);

    Route::get('/routines/list', [RoutineController::class, 'list'])->name('routines.list');
    Route::resource('routines', RoutineController::class)->except(['index']);

    Route::put('/users/{id}/meta', [UserController::class, 'meta'])->name('users.meta');
    Route::get('/users/import', [UserController::class, 'import'])->name('users.import');
    Route::post('/users/import', [UserController::class, 'upload'])->name('users.upload');
    Route::get('/users/list', [UserController::class, 'list'])->name('users.list');
});
Route::middleware('cache.headers:public;max_age=300;etag')->group(function () {
    Route::resource('attendances', AttendanceController::class)->except(['show']);
    Route::get('/attendances/{hsc}/{roll}/{name?}', [AttendanceController::class, 'show'])->name('attendances.show');

    Route::get('/facebook/login', [FacebookController::class, 'login'])->name('facebook.login');
    Route::get('/facebook/callback', [FacebookController::class, 'callback'])->name('facebook.callback');
    Route::get('/facebook/stories', [FacebookController::class, 'fetchStories'])->name('facebook.stories');

    Route::get('/wall', [AppController::class, 'wall'])->name('app.wall');
});
Route::middleware('cache.headers:public;max_age=604800;etag')->group(function () {
    Route::get('/', [AppController::class, 'home'])->name('app.home');
    Route::get('/about', [AppController::class, 'about'])->name('app.about');
    Route::get('/academic-rules', [AppController::class, 'academicRules'])->name('academic-rules');
    Route::get('blog', [BlogController::class, 'index'])->name('blog.index');
    Route::get('/blog/{id}/{slug?}', [BlogController::class, 'show'])->name('blog.show');
    Route::get('/c', [AppController::class, 'c'])->name('app.c');
    Route::get('/cache', [AccessoriesController::class, 'cache'])->name('cache');
    Route::resource('comments', CommentController::class);
    Route::get('/contact', [AppController::class, 'contact'])->name('app.contact');
    Route::get('/falling-letter', [GameController::class, 'fallingLetter'])->name('app.falling-letter');
    Route::get('/image/{w}/{h}/{filename}', [ImageController::class, 'showResizedImage']);
    Route::resource('likes', LikeController::class);
    Route::get('/principals-message', [AppController::class, 'principal'])->name('app.principal');
    Route::get('/privacy-policy', [AppController::class, 'privacyPolicy'])->name('app.privacy-policy');
    Route::get('/sitemap.xml', [AccessoriesController::class, 'sitemap'])->name('sitemap');
    Route::get('/staffs', [UserController::class, 'staffs'])->name('users.staffs');
    Route::get('/students', [UserController::class, 'students'])->name('users.students');
    Route::get('/teachers', [UserController::class, 'teachers'])->name('users.teachers');
    Route::get('/vice-principals-message', [AppController::class, 'vice'])->name('app.vice');
    /*
    * Overwriting the controllers in admin group
    */
    Route::resource('users', UserController::class)->except('show');
    Route::resource('status', PostController::class);
    Route::get('/notices/{id}/{slug?}', [NoticeController::class, 'show'])->name('notices.show');
    Route::get('/results', [ResultController::class, 'index'])->name('results.index');
    Route::get('/routines', [RoutineController::class, 'index'])->name('routines.index');
});

require __DIR__.'/auth.php';
Route::get('/clear-cache', [AccessoriesController::class, 'clearCache'])->name('clear-cache');
Route::get('/generate-text', [HuggingFaceController::class, 'chat']);
Route::get('/sync', [SyncController::class, 'sync']);


Route::middleware('cache.headers:public;max_age=60;etag')->group(function () {
    Route::get('/users/{id}/{name?}', [UserController::class, 'show'])->name('users.show');
});