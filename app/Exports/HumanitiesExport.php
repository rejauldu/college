<?php

namespace App\Exports;

use App\Models\Result;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class HumanitiesExport implements FromView
{
    protected $request;

    function __construct($request) {
        $this->request = $request;
    }
    public function view(): View
    {
        return view('results.humanities', [
            'results' => Result::where('hsc', $this->request->batch)
                ->where('exam_type_id', $this->request->exam_type_id)
                ->whereBetween('roll', [1, 499])
                ->whereHas('user', function($q) {
                    $q->where('status', 1);
                })
                ->with('user')
                ->orderBy('roll')->get()
        ]);
    }
}