<?php

use Illuminate\Support\Str;
/**
 * Function getSVG
 * Include svg image with a span wrapper
 *
 * @param string $imagePath [explicits description]
 * @param string $class     [explicits description]
 *
 * @return void
 */
 use Carbon\Carbon;

function getSVG(string $imagePath, string $class = '') {
	if (!$imagePath) {
		return null;
	}
	$publicPath = env('APP_PUBLIC_PATH', 'public_html');

	echo '<i class="block ' . $class . '">';
	include base_path($publicPath) . DIRECTORY_SEPARATOR . $imagePath;
	echo '</i>';
}

function appMeta($arr) {
    $metas = App\Models\AppMeta::select('meta_key', 'meta_value')
        ->whereIn('meta_key', $arr)
        ->get()->pluck('meta_value', 'meta_key')->toArray();
    return (object)$metas;
}
function userMeta($arr, $id = null) {
	if(!$id) {
		$id = auth()->id();
	}
    $metas = App\Models\UserMeta::select('meta_key', 'meta_value')
        ->whereIn('meta_key', $arr)
		->where('user_id', $id)
        ->get()->pluck('meta_value', 'meta_key')->toArray();
    return (object)$metas;
}
function postMeta($arr, $id) {
    $metas = App\Models\PostMeta::select('meta_key', 'meta_value')
        ->whereIn('meta_key', $arr)
		->where('user_id', $id)
        ->get()->pluck('meta_value', 'meta_key')->toArray();
    return (object)$metas;
}
function e2b($englishString) {
    // Mapping of English digits to Bengali digits
    $englishToBengaliDigits = [
        '0' => '০',
        '1' => '১',
        '2' => '২',
        '3' => '৩',
        '4' => '৪',
        '5' => '৫',
        '6' => '৬',
        '7' => '৭',
        '8' => '৮',
        '9' => '৯'
    ];

    // Replace English digits with Bengali digits
    $bengaliString = strtr($englishString, $englishToBengaliDigits);

    return $bengaliString;
}
function b2e($bengaliString) {
    // Mapping of Bengali digits to English digits
    $bengaliToEnglishDigits = [
        '০' => '0',
        '১' => '1',
        '২' => '2',
        '৩' => '3',
        '৪' => '4',
        '৫' => '5',
        '৬' => '6',
        '৭' => '7',
        '৮' => '8',
        '৯' => '9'
    ];

    // Replace Bengali digits with English digits
    $englishString = strtr($bengaliString, $bengaliToEnglishDigits);

    return $englishString;
}
if (! function_exists('clickable')) {
    function clickable($text) {
        // Regular expression to find URLs in the text
        $urlPattern = '/(https?:\/\/[^\s]+)/';
        
        // Replace URLs with anchor tags
        $textWithLinks = preg_replace($urlPattern, '<a class="underline hover:text-black" href="$1" target="_blank">$1</a>', $text);
        
        return $textWithLinks;
    }
}

if (! function_exists('formattedDateOrDifference')) {
    function formattedDateOrDifference($date) {
        $carbonDate = Carbon::parse($date);

        if ($carbonDate->diffInHours(Carbon::now()) > 24) {
            // More than 24 hours, return formatted date
            return $carbonDate->format('jS M Y');
        } else {
            // Within 24 hours, return relative time difference
            return $carbonDate->shortRelativeDiffForHumans();
        }
    }
}
if (! function_exists('year')) {
    function year($user) {
        if($user->batch && $user->batch->meta_key == "batch1")
            return "২য় বর্ষ";
        elseif($user->batch && $user->batch->meta_key == "batch2")
            return "১ম বর্ষ";
        else
            return "এইচএসসি ".e2b($user->hsc)." ব্যাচ";
    }
}
if (! function_exists('group')) {
    function group($roll) {
        if($roll>600) {
            return "ব্যবসায় শিক্ষা";
        } elseif($roll>500) {
            return "বিজ্ঞান";
        } else {
            return "মানবিক";
        }
    }
}
if (! function_exists('fname')) {
    function fname($name) {
        $search = array("md", "md.", "mst", "mst.", "MD", "MD.", "Md", "Md.", "MST", "MST.", "মো", "মো:", "মোসা", "মিস", "মোসা:", "মিস:");

        // Replacement string (empty)
        $replace = "";

        // Perform the replacement
        $output = str_replace($search, $replace, $name);
        $output = trim($output);
        $words = explode(' ', $output);

        // Get the first word (trim it to remove any leading or trailing whitespace)
        $first_word = isset($words[0]) ? trim($words[0]) : '';

        // Output the first word
        return $first_word;
    }
}
if (! function_exists('likes')) {
    function likes($post, $like) {
        $id = auth()->id();
        if($like == 1)
            foreach($post->likes as $l) {
                if($l->user->id == $id) {
                    return true;
                }
            }
        if($like == 2)
            foreach($post->dislikes as $dis) {
                if($dis->user->id == $id) {
                    return true;
                }
            }
        return false;
    }
}
function ordinal($number=0) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return 'th';
    else
        return $ends[$number % 10];
}

function excerpt($text, $words = 30) {
    return Str::words($text, $words, '...');
}