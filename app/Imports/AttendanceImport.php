<?php

namespace App\Imports;

use App\Models\Attendance;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AttendanceImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Collection|null
    */
    public function collection(Collection  $rows)
    {
        $request = request()->all();
        foreach($rows as $row) {
            Attendance::create([
                'roll' => $row['roll'],
                'hsc' => $row['hsc'] ?? $request['hsc'],
                'type' => $row['type'] ?? $request['type'],
                'created_at' => Carbon::createFromFormat('m/d/Y', $row['created_at'] ?? $request['created_at']),
            ]);
        }
        
    }
}
