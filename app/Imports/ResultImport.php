<?php

namespace App\Imports;

use App\Models\Result;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ResultImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Collection|null
    */
    public function collection(Collection  $rows)
    {
        $request = request()->all();
        foreach($rows as $row) {
            if($row['hsc'] && $row['roll']) {
                Result::create([
                    'bangla_cq' => $row['bangla_cq'],
                    'bangla_mcq' => $row['bangla_mcq'] ?? null,
                    'english' => $row['english'],
                    'ict_cq' => $row['ict_cq'],
                    'ict_mcq' => $row['ict_mcq'],
                    'ict_prac' => $row['ict_prac'] ?? null,
                    'agri_cq' => $row['agri_cq'] ?? null,
                    'agri_mcq' => $row['agri_mcq'] ?? null,
                    'agri_prac' => $row['agri_prac'] ?? null,
                    'social_cq' => $row['social_cq'] ?? null,
                    'social_mcq' => $row['social_mcq'] ?? null,
                    'political_cq' => $row['political_cq'] ?? null,
                    'political_mcq' => $row['political_mcq'] ?? null,
                    'islamic_cq' => $row['islamic_cq'] ?? null,
                    'islamic_mcq' => $row['islamic_mcq'] ?? null,
                    'logic_cq' => $row['logic_cq'] ?? null,
                    'logic_mcq' => $row['logic_mcq'] ?? null,
                    'geography_cq' => $row['geography_cq'] ?? null,
                    'geography_mcq' => $row['geography_mcq'] ?? null,
                    'geography_prac' => $row['geography_prac'] ?? null,
                    'philosophy_cq' => $row['philosophy_cq'] ?? null,
                    'philosophy_mcq' => $row['philosophy_mcq'] ?? null,
                    'economics_cq' => $row['economics_cq'] ?? null,
                    'economics_mcq' => $row['economics_mcq'] ?? null,
                    'physics_cq' => $row['physics_cq'] ?? null,
                    'physics_mcq' => $row['physics_mcq'] ?? null,
                    'physics_prac' => $row['physics_prac'] ?? null,
                    'chemistry_cq' => $row['chemistry_cq'] ?? null,
                    'chemistry_mcq' => $row['chemistry_mcq'] ?? null,
                    'chemistry_prac' => $row['chemistry_prac'] ?? null,
                    'math_cq' => $row['math_cq'] ?? null,
                    'math_mcq' => $row['math_mcq'] ?? null,
                    'math_prac' => $row['math_prac'] ?? null,
                    'biology_cq' => $row['biology_cq'] ?? null,
                    'biology_mcq' => $row['biology_mcq'] ?? null,
                    'biology_prac' => $row['biology_prac'] ?? null,
                    'accounting_cq' => $row['accounting_cq'] ?? null,
                    'accounting_mcq' => $row['accounting_mcq'] ?? null,
                    'management_cq' => $row['management_cq'] ?? null,
                    'management_mcq' => $row['management_mcq'] ?? null,
                    'finance_cq' => $row['finance_cq'] ?? null,
                    'finance_mcq' => $row['finance_mcq'] ?? null,
                    'exam_type_id' => $row['exam_type_id'] ?? $request['exam_type_id'],
                    'roll' => $row['roll'] ?? null,
                    'hsc' => $row['hsc'] ?? null,
                ]);
            }
        }
    }
}