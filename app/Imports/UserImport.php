<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class UserImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Collection|null
    */
    public function collection(Collection  $rows)
    {
        $request = request()->all();
        $phone = '';
        foreach($rows as $row) {
            $phone = $this->getPhone($row['phone']);
            User::create([
                'name' => $row['name'] ?? '',
                'email' => $row['email'] ?? null,
                'password' => Hash::make($row['password'] ?? $phone),
                'phone' => $phone,
                'photo' => $row['photo'] ?? 'avatar.webp',
                'roll' => $row['roll'] ?? 0,
                'ssc' => $row['ssc'] ?? 0,
                'hsc' => $row['hsc'] ?? 0,
                'bcs' => $row['bcs'] ?? 0,
                'subject_id' => $row['subject_id'] ?? 8,
                'status' => $row['status'] ?? 1,
                'role' => $row['role'] ?? 2,
                'note' => $row['note'] ?? null,
                'email_verified_at' => Carbon::now(),
            ]);
        }

    }

    public function getPhone($p) {
        return strlen($p)<11?'0'.$p:$p;
    }
}
