<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AccessoriesController;
use App\Http\Controllers\Image2WebpController;
use Artisan;
use Auth;
use App\Models\Image;
use App\Models\Post;
use App\Models\PostMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Session;

class NoticeController extends Controller {
    protected $converter, $accessories;
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'show'
        ]]);
        $this->accessories = new AccessoriesController();
        $this->converter = new Image2WebpController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $posts = Post::where('type', 'notice')->latest()->paginate(10);
        return view('notices.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::where('type', 'notice')->latest()->get();
        return view('notices.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $user = auth()->user();
        $data = $request->except('_token', '_method');
        $data['type'] = "notice";
        $data['user_id'] = $user->id;
        $post = Post::create($data);
        $this->associateWithNotice($post->id);
        
        if(isset($data['is_moving'])) {
            PostMeta::create([
                'meta_key'      => 'is_moving',
                'meta_value'    => 1,
                'post_id'       =>$post->id
            ]);
        } else {
            PostMeta::create([
                'meta_key'      => 'is_moving',
                'meta_value'    => 0,
                'post_id'       =>$post->id
            ]);
        }
        if(isset($data['notice_cat'])) {
            PostMeta::create([
                'meta_key'      => 'notice_cat',
                'meta_value'    => $data['notice_cat'],
                'post_id'       =>$post->id
            ]);
        } else {
            PostMeta::create([
                'meta_key'      => 'notice_cat',
                'meta_value'    => 0,
                'post_id'       =>$post->id
            ]);
        }
        
        //clear cache
        $this->accessories->clearCache();
        $this->accessories->cache();
        
        return redirect(route('notices.list'))->with('message', 'Notice added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::with('images')->where('id', $id)->first();
        return view('notices.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::with(['user', 'images', 'metas' => function ($q) {
            $q->where('meta_key', 'is_moving')
                ->orWhere('meta_key', 'notice_cat');
        }])->where('id', $id)->first();
        
        return view('notices.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        
        if(!isset($data['is_moving'])) {
            $data['is_moving'] = 0;
        }
        $post = Post::find($id);
        $post->update($data);
        if(isset($data['is_moving'])) {
            $postMeta = PostMeta::where('post_id', $post->id)->where('meta_key', 'is_moving')->first();
            if($data['is_moving'] == 1) {
                $postMeta->update([
                    'meta_value'    => 1
                ]);
            } else {
                $postMeta->update([
                    'meta_value'    => 0
                ]);
            }
        }
        
        $postMeta = PostMeta::where('post_id', $post->id)->where('meta_key', 'notice_cat')->first();
        if(isset($data['notice_cat'])) {
            $postMeta->update([
                'meta_value'    => $data['notice_cat']
            ]);
        } else {
            $postMeta->update([
                'meta_value'    => 0
            ]);
        }
        
        //clear cache
        $this->accessories->clearCache();
        $this->accessories->cache();
        
        return redirect(route('notices.list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = 'posts/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Notice has been deleted');
    }

    private function recache() {
        $prefix = 'notices';
        Cache::forget($prefix);

        for ($i=1; $i < 1000; $i++) {
            $key = $prefix . $i;
            if (Cache::has($key)) {
                Cache::forget($key);
            } else {
                break;
            }
        }

        $posts = Post::where('type', 'notice')->with('metas')->latest()->paginate(10);
        Cache::put('notices', $posts);
        Cache::put('notices.1', $posts);
    }
    
    /**
     * Link the temporary images to a notice after form submission.
     */
    public function associateWithNotice($noticeId)
    {
        
        // Retrieve the uploaded file IDs from the session
        $uploadedFileIds = Session::get('uploaded_files', []);

        if (!empty($uploadedFileIds)) {
            // Update the images to set `is_temporary` to false and link to the notice
            Image::whereIn('id', $uploadedFileIds)->update([
                'is_temporary' => false,
                'post_id' => $noticeId
            ]);

            // Clear the session data after association
            Session::forget('uploaded_files');
        }
    }

}