<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Image2WebpController;
use Auth;
use App\Models\Post;
use Illuminate\Support\Facades\File;

class PostController extends Controller {
    protected $converter;
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'show'
        ]]);
        $this->converter = new Image2WebpController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        return view('app.wall', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::orderBy('id', 'desc')->get();
        return view('notices.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except('_token', '_method');
        $file = $request->file('link');
        if($file) {
            $ext = $file->extension();
            if ($ext == 'pdf') {
                $destination_path = 'posts';
                $new_name = time() . '-pdf.pdf';
                $file->move($destination_path, $new_name);
                $data['link'] = $new_name;
            } else {
                $destination_path = 'posts';
                $only_name = time() . '-image'; 
                $new_name = $only_name . '.' . $file->getClientOriginalExtension();
                $file->move($destination_path, $new_name);
                $this->converter->convert($destination_path.'/'.$new_name);
                $data['link'] = $only_name . '.webp';
            }
        }
        
        $user = auth()->user();
        $data['type'] = "status";
        $data['user_id'] = $user->id;
        Post::create($data);
        return redirect(route('app.wall'))->with('message', 'Status added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        return view('notices.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::with(['user'])->where('id', $id)->first();
        $user = auth()->user();
        if($user->id == $post->user->id)
            return view('posts.create', compact('post'));
        return redirect()->back()->with('message', 'You are not authorized to update this post');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        $post = Post::find($id);
        $file = $request->file('link');
        if($file) {
            $ext = $file->extension();
            $directory = 'posts/';
            if ($ext == 'pdf') {
                $destination_path = 'posts';
                $new_name = time() . '-pdf.pdf';
                $file->move($destination_path, $new_name);
                $data['link'] = $new_name;
                File::delete($directory . $post->link);
            } else {
                $destination_path = 'posts';
                $only_name = time() . '-image'; 
                $new_name = $only_name . '.' . $file->getClientOriginalExtension();
                $file->move($destination_path, $new_name);
                $this->converter->convert($destination_path.'/'.$new_name);
                $data['link'] = $only_name . '.webp';
                File::delete($directory . $post->link);
            }
        }
        $user = auth()->user();
        if($user->id == $post->user->id)
            $post->update($data);
        return redirect()->back()->with('message', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = '/posts/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Post has been deleted');
    }

}