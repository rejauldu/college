<?php

namespace App\Http\Controllers;

use App\Models\FallingLetter;

class GameController extends Controller
{
    public function fallingLetter() {
        return view('tools.falling-letter');
    }
}
