<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Image2WebpController;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class RoutineController extends Controller {
    protected $converter;
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'index'
        ]]);
        $this->converter = new Image2WebpController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (Cache::has('routine')) {
            $posts = Cache::get('routine');
        } else {
            $posts = Post::where('type', 'routine')->get();
            Cache::put('routine', $posts);
        }
        return view('routines.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::where('type', 'routine')->get();
        return view('routines.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('routines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except('_token', '_method');
        $file = $request->file('link');
        if ($file) {
            $destination_path = 'posts';
            $only_name = time() . '-image'; 
            $new_name = $only_name . '.' . $file->getClientOriginalExtension();
            $file->move($destination_path, $new_name);
            $this->converter->convert($destination_path.'/'.$new_name, 1000, 562, -1);
            $data['link'] = $only_name . '.webp';
        }
        $data['type'] = 'routine';
        $post = Post::create($data);
        $this->recache();
        return redirect(route('routines.list'))->with('message', 'Routine added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        return view('routines.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::find($id);
        return view('routines.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        $post = Post::find($id);
        $directory = 'posts/';
        $file = $request->file('link');
        if ($file) {
            $destination_path = 'posts';
            $only_name = time() . '-image'; 
            $new_name = $only_name . '.' . $file->getClientOriginalExtension();
            $file->move($destination_path, $new_name);
            $this->converter->convert($destination_path.'/'.$new_name, 1000, 562);
            $data['link'] = $only_name . '.webp';
            File::delete($directory . $post->link);
        }
        $post->update($data);
        $this->recache();
        return redirect(route('routines.list'))->with('message', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = 'posts/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Routine has been deleted');
    }
    private function recache() {
        Cache::forget('routine');
        $posts = Post::where('type', 'routine')->get();
        Cache::put('routine', $posts);
    }
}