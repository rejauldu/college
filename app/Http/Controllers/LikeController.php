<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;

class LikeController extends Controller {
    
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $posts = Post::where('type', 'notice')->latest()->paginate(10);
        return view('notices.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::where('type', 'notice')->latest()->get();
        return view('notices.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $user = auth()->user();
        $post = Like::where('user_id', $user->id)->where('post_id', $request->post_id)->first();
        if($request->react == 1) {
            if($post && $post->react == 1) {
                $post->update(['react' => 0]);
            } elseif($post) {
                $post->update(['react' => 1]);
            } else {
                Like::create(['react' => 1, 'post_id' => $request->post_id, 'user_id' => $user->id]);
            }
        } else {
            if($post && $post->react == 2) {
                $post->update(['react' => 0]);
            } elseif($post) {
                $post->update(['react' => 2]);
            } else {
                Like::create(['react' => 2, 'post_id' => $request->post_id, 'user_id' => $user->id]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        return view('notices.show', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = 'posts/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Notice has been deleted');
    }

}