<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Image2WebpController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Models\Image;
use Intervention\Image\Encoders\WebpEncoder;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Gd\Driver as GdDriver;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class ImageController extends Controller
{
    protected $converter;
    public function __construct() {
        $this->converter = new Image2WebpController();
    }
    /**
     * Handle the image upload and store it as a temporary image.
     */
    public function store(Request $request)
    {
        if ($request->hasFile('files')) {
            $uploadedFiles = [];

            foreach ($request->file('files') as $file) {
                // Store the file in the 'public/uploads' directory
                $destination_path = 'posts/uploads';
                $original = $file->getClientOriginalName();
                $only_name = $original ."-". time(); 
                $new_name = $only_name . '.' . $file->getClientOriginalExtension();
                $file->move($destination_path, $new_name);
                $this->converter->convert($destination_path.'/'.$new_name, 0, 0, 15);

                // Save the image record in the database as temporary
                $image = Image::create([
                    'name' => $only_name . '.webp',
                    'is_temporary' => true, // Mark it as temporary
                ]);

                // Save the uploaded file ID to the session
                $ids[] = $image->id;
                $names[] = $only_name . '.webp';
            }

            // Store the array of temporary file IDs in session for later use
            Session::put('uploaded_files', $ids);

            return response()->json([
                'success' => true,
                'ids' => $ids,
                'names' => $names
            ]);
        }

        return response()->json(['success' => false, 'message' => 'No files uploaded.']);
    }

    /**
     * Scheduled job to clean up unused temporary images.
     * This can be called by a scheduled job in the Laravel console kernel.
     */
    public function destroy()
    {
        // Define the expiration time (e.g., 24 hours)
        $expiration = now()->subDay();

        // Find temporary images older than the expiration time and delete them
        $images = Image::where('is_temporary', true)
            ->where('created_at', '<', $expiration)
            ->get();

        foreach ($images as $image) {
            // Delete the file from posts/uploads
            $directory = 'posts/uploads/';
            File::delete($directory . $image->name);
            $image->delete();
        }
    }

    public function showResizedImage($w=300, $h=300, $filename) {
        $path = public_path("/posts/{$filename}");

        if (!file_exists($path)) {
            $googleDriveUrl = "https://drive.google.com/uc?export=view&id=$filename";
            $response = Http::get($googleDriveUrl);
            // Check if the image is fetched successfully
            if ($response->successful()) {
                // Get the image content from Google Drive
                $path = $response->body();
            } else {
                // Handle the case where the image could not be fetched
                abort(404, 'Image not found on Google Drive or local server.');
            }
        }

        // Create an instance of ImageManager with GD driver
        $manager = new ImageManager(new GdDriver());
        
        // Read and resize image while maintaining aspect ratio
        $image = $manager->read($path)->resize($w, $h, function ($constraint) {
            $constraint->aspectRatio();
        });

        // Encode the image properly (use JpegEncoder instead of 'jpg')
        $encodedImage = $image->encode(new WebpEncoder());

        // Return the response with correct content type
        return response($encodedImage, 200)->header("Content-Type", "image/webp");
    }
}