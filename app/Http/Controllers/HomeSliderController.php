<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Image2WebpController;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class HomeSliderController extends Controller {
    protected $converter;
    public function __construct() {
        $this->middleware('admin');
        $this->converter = new Image2WebpController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        return view('home-sliders.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::where('type', 'home-slider')->latest()->get();
        return view('home-sliders.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('home-sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except('_token', '_method');
        $file = $request->file('link');
        if ($file) {
            $destination_path = 'posts';
            $only_name = time() . '-image'; 
            $new_name = $only_name . '.' . $file->getClientOriginalExtension();
            $file->move($destination_path, $new_name);
            $this->converter->convert($destination_path.'/'.$new_name, 1000, 562);
            $data['link'] = $only_name . '.webp';
        }
        $data['type'] = 'home-slider';
        $data['user_id'] = auth()->id();
        $post = Post::create($data);
        $this->recache();
        return redirect(route('home-sliders.list'))->with('message', 'Slider added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        return view('home-sliders.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::find($id);
        return view('home-sliders.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        $post = Post::find($id);
        $directory = 'posts/';
        $file = $request->file('link');
        if ($file) {
            $destination_path = 'posts';
            $only_name = time() . '-image'; 
            $new_name = $only_name . '.' . $file->getClientOriginalExtension();
            $file->move($destination_path, $new_name);
            $this->converter->convert($destination_path.'/'.$new_name, 1000, 562);
            $data['link'] = $only_name . '.webp';
            File::delete($directory . $post->link);
        }
        $post->update($data);
        $this->recache();
        return redirect(route('home-sliders.list'))->with('message', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = 'posts/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Slider has been deleted');
    }
    private function recache() {
        Cache::forget('home');
        $data['slides'] = Post::where('type', 'home-slider')->latest()->get();
        $data['movings'] = Post::where('type', 'notice')->with('metas')->latest()->get();
        $data['principal'] = User::where('role', config('auth.role')['principal'])->with('userMeta')->where('status', 1)->first();
        $data['vice'] = User::where('role', config('auth.role')['vice'])->with('userMeta')->where('status', 1)->first();
        Cache::put('home', $data);
    }
}