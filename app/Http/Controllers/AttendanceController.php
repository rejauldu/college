<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Attendance;
use App\Models\AppMeta;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AttendanceController extends Controller {
    public function __construct() {
        $this->middleware('auth', ['except' => [
            'store', 'index', 'show'
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $auth = auth()->user();
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        $users = User::where("hsc", ">", 2024)->where("photo", "<>", "avatar.webp");
        $search = false;
        if($request->search) {
            $search = b2e($request->search);
            if($auth && $auth->role == 2) {
                $users = User::with('subject', 'userMeta')->where('hsc', $auth->hsc)->where("roll", $search);
            } else {
                $users = User::with('subject', 'userMeta')
                    ->wherein('hsc', [$batches->batch1, $batches->batch2])
                    ->where("roll", $search);
            }
            $users = $users->paginate(10);
            $search = $request->search;
        } elseif($auth) {
            $selfRow = User::where('id', $auth->id);
            $users = $users->where("id", "<>", $auth->id);
            $users = $selfRow->union($users)->orderByRaw("id = {$auth->id} DESC")->inRandomOrder()->paginate(20);
        } else {
            $users = $users->inRandomOrder()->paginate(20);
        }
        return view('attendances.index', compact('users', 'search'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list() {
        $posts = Post::orderBy('id', 'desc')->get();
        return view('notices.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // Validate that the password is correct
        if ($request->password != 'hasanpursncollege') {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // Iterate over each attendance record and create it in the database
        foreach ($request->attendances as $attendanceData) {
            Attendance::create($attendanceData);
        }

        return response()->json(['message' => 'Attendances stored successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hsc, $roll) {
        if($hsc < 2025 || $roll == 0) {
            return redirect()->back()->with("message", "এই ব্যবহারকারীর হাজিরা দেখা যাবে না");
        }
        $user = User::where('hsc', $hsc)->where('roll', $roll)->first();
        
        return $this->gotoAttendance($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::find($id);
        return view('notices.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        if(!isset($data['is_moving'])) {
            $data['is_moving'] = 0;
        }
        $post = Post::find($id);
        $directory = '/notices/';
        $pdf = $request->file('pdf');
        if ($pdf) {
            $destination_path = 'assets/notices';
            $new_name = time() . '-pdf.' . $pdf->getClientOriginalExtension();
            $pdf->move($destination_path, $new_name);
            $data['pdf'] = $new_name;
            File::delete($directory . $post->pdf);
        }
        $post->update($data);
        return redirect(route('notices.list'))->with('message', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = '/notices/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Post has been deleted');
    }
    
    public function dateFrom() {
        $dates = appMeta([
            'dateFrom1',
            'dateFrom2'
        ]);
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        return view('attendances.date-from', compact('dates', 'batches'));
    }
    public function dateFromStore(Request $request) {
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        $dateFrom = '';
        foreach($batches as $k => $v) {
            if($request->batch == $v) {
                if($k == 'batch1') {
                    $dateFrom = 'dateFrom1';
                } elseif($k == 'batch2') {
                    $dateFrom = 'dateFrom2';
                }
            }
        }
        if($dateFrom) {
            AppMeta::updateOrCreate(
            [
                'meta_key'   => $dateFrom
            ],
            [
                'meta_value' => $request->date_from
            ]);
        }
        return redirect()->back()->with('message', 'Date updated');
    }
    private function gotoAttendance($user) {
        $posts = Attendance::select(
                DB::raw('DATE(created_at) as date'),
                DB::raw('MAX(CASE WHEN roll = ' . $user->roll . ' THEN roll ELSE 0 END) as roll')
            )
            ->where('hsc', $user->hsc)
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('date', 'desc')
            ->paginate(50);
        
        $dates = appMeta([
            'dateFrom1',
            'dateFrom2'
        ]);
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        
        $dateFrom = '';
        if($batches->batch1 == $user->hsc) {
            $dateFrom = 'dateFrom1';
        } elseif($batches->batch2 == $user->hsc) {
            $dateFrom = 'dateFrom2';
        }
        
        $date_from = new \DateTime($dates->$dateFrom);
        
        return view('attendances.show', compact('posts', 'user', 'date_from'));
    }

}