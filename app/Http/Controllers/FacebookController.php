<?php
namespace App\Http\Controllers;

use Facebook\Facebook;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    protected $fb;

    public function __construct()
    {
        $this->fb = new Facebook([
            'app_id' => config('facebook.app_id'),
            'app_secret' => config('facebook.app_secret'),
            'default_graph_version' => 'v16.0',
        ]);
    }

    public function login()
    {
        $helper = $this->fb->getRedirectLoginHelper();
        $permissions = ["user_birthday", "user_gender", "user_photos", "user_posts", "user_videos"]; // Adjust based on required permissions
        $loginUrl = $helper->getLoginUrl(config('facebook.redirect_uri'), $permissions);
        return redirect($loginUrl);
    }

    public function callback(Request $request)
    {
        $helper = $this->fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
            session(['fb_access_token' => (string) $accessToken]);
            return redirect()->route('facebook.stories');
        } catch (\Exception $e) {
            return back()->with('error', 'Unable to log in with Facebook.');
        }
    }

    public function fetchStories()
    {
        $accessToken = session('fb_access_token');
        if (!$accessToken) {
            return redirect()->route('facebook.login');
        }

        try {
            $response = $this->fb->get('/me/stories?fields=id,story,message,attachments', $accessToken);
            $stories = $response->getDecodedBody();
            return view('facebook.stories', compact('stories'));
        } catch (\Exception $e) {
            return back()->with('error', 'Unable to fetch stories.');
        }
    }
}
