<?php

namespace App\Http\Controllers;

use Auth;
use App\Exports\CommerceExport;
use App\Exports\HumanitiesExport;
use App\Exports\ScienceExport;
use App\Imports\ResultImport;
use App\Models\Batch;
use App\Models\ExamType;
use App\Models\Group;
use App\Models\Result;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ResultController extends Controller {
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'index', 'api'
        ]]);
        $this->middleware('auth', ['only' => [
            'index', 'api'
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $exam_types = ExamType::all();
        return view('results.index', compact('exam_types'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function api(Request $request) {
        $user = auth()->user();
        $result = Result::where('roll', $user->roll)
            ->where('hsc', $user->hsc)
            ->where('exam_type_id', $request->exam_type_id)
            ->whereHas('user', function($q) {
                $q->where('status', 1);
            })
            ->with('user.subject', 'type')
            ->first();
        if($result) {
            $data['result'] = $this->getData($result);
            $data['type'] = $result->type->name;
            $data['type_id'] = $result->type->id;
            $data['user'] = $result->user;
            $data['status'] = 200;
        } else {
            $data['status'] = 500;
        }
        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request) {
        $data = [];
        $results = Result::with('user');
        if($request->phone) {
            $results = $results->whereHas('user', function($q) use($request) {
                $q->where('phone', 'like', '%'.$request->phone.'%');
            });
            $data['phone'] = $request->phone;
        }
        if($request->roll) {
            $results = $results->whereHas('user', function($q) use($request) {
                $q->where('roll', $request->roll);
            });
            $data['roll'] = $request->roll;
        }
        if($request->hsc) {
            $results = $results->whereHas('user', function($q) use($request) {
                $q->where('hsc', 'like', '%'.$request->hsc.'%');
            });
            $data['hsc'] = $request->hsc;
        }

        $data['results'] = $results->paginate(100);
        return view('results.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except('_token', '_method');
        if(!isset($data['is_moving'])) {
            $data['is_moving'] = 0;
        }
        $pdf = $request->file('pdf');
        if ($pdf) {
            $destination_path = 'notices';
            $new_name = time() . '-pdf.pdf';
            $pdf->move($destination_path, $new_name);
            $data['pdf'] = $new_name;
        }
        Post::create($data);
        return redirect(route('notices.list'))->with('message', 'Post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);
        return view('notices.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result) {
        $result = (object)$result->toArray();
        return view('results.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->except('_token', '_method');
        $result = Result::find($id);
        $result->update($data);
        return redirect(route('results.list'))->with('message', 'Result updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::find($id);
        $directory = 'notices/';
        File::delete($directory . $post->pdf);
        $post->delete();
        return redirect()->back()->with('message', 'Post has been deleted');
    }

    /**
     * Import resources from excel.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function import() {
        $data ['exam_types'] = ExamType::all();
        return view('results.import', $data);
    }
    public function upload(Request $request) {
        set_time_limit(1200);
        
        $extensions = array("xls","xlsx","csv");

        $result = array($request->file('results')->getClientOriginalExtension());

        if(in_array($result[0],$extensions)){
            Excel::import(new ResultImport, $request->file('results'), 12);
        }else{
            request()->validate([
                'results' => 'required|mimes:csv, xls, xlsx|max:2048'
            ]);
        }
        
        return redirect()->back()->with('message', 'Result Imported');
    }
    public function getData($result) {
        $data['Bangla']['cq'] = $result->bangla_cq;
        $data['Bangla']['mcq'] = $result->bangla_mcq;
        $data['English']['cq'] = $result->english;
        $data['Information & Comm. Technology']['cq'] = $result->ict_cq;
        $data['Information & Comm. Technology']['mcq'] = $result->ict_mcq;
        $data['Information & Comm. Technology']['prac'] = $result->ict_prac;

        $data['Physics']['cq'] = $result->physics_cq;
        $data['Physics']['mcq'] = $result->physics_mcq;
        $data['Physics']['prac'] = $result->physics_prac;

        $data['Chemistry']['cq'] = $result->chemistry_cq;
        $data['Chemistry']['mcq'] = $result->chemistry_mcq;
        $data['Chemistry']['prac'] = $result->chemistry_prac;

        $data['Higher Math']['cq'] = $result->math_cq;
        $data['Higher Math']['mcq'] = $result->math_mcq;
        $data['Higher Math']['prac'] = $result->math_prac;

        $data['Biology']['cq'] = $result->biology_cq;
        $data['Biology']['mcq'] = $result->biology_mcq;
        $data['Biology']['prac'] = $result->biology_prac;

        $data['Agriculture']['cq'] = $result->agri_cq;
        $data['Agriculture']['mcq'] = $result->agri_mcq;
        $data['Agriculture']['prac'] = $result->agri_prac;

        $data['Social Work']['cq'] = $result->social_cq;
        $data['Social Work']['mcq'] = $result->social_mcq;

        $data['Political Science']['cq'] = $result->political_cq;
        $data['Political Science']['mcq'] = $result->political_mcq;

        $data['Islamic History']['cq'] = $result->islamic_cq;
        $data['Islamic History']['mcq'] = $result->islamic_mcq;

        $data['Logic']['cq'] = $result->logic_cq;
        $data['Logic']['mcq'] = $result->logic_mcq;

        $data['Geography']['cq'] = $result->geography_cq;
        $data['Geography']['mcq'] = $result->geography_mcq;
        $data['Geography']['prac'] = $result->geography_prac;

        $data['Philosophy']['cq'] = $result->philosophy_cq;
        $data['Philosophy']['mcq'] = $result->philosophy_mcq;
        
        $data['Accounting']['cq'] = $result->accounting_cq;
        $data['Accounting']['mcq'] = $result->accounting_mcq;

        $data['Management']['cq'] = $result->management_cq;
        $data['Management']['mcq'] = $result->management_mcq;

        $data['Finance']['cq'] = $result->finance_cq;
        $data['Finance']['mcq'] = $result->finance_mcq;

        $data['Economics']['cq'] = $result->economics_cq;
        $data['Economics']['mcq'] = $result->economics_mcq;

        return $data;
    }
    /**
     * Export resources from excel.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function export() {
        $data ['exam_types'] = ExamType::all();
        $data ['groups'] = Group::all();
        $data ['batches'] = appMeta([
            'batch1',
            'batch2'
        ]);
        return view('results.export', $data);
    }
    public function download(Request $request) {
        if($request->group_id == 1)
            return Excel::download(new ScienceExport($request), 'science.xlsx');
        elseif($request->group_id == 2)
            return Excel::download(new HumanitiesExport($request), 'humanities.xlsx');
        elseif($request->group_id == 3)
            return Excel::download(new CommerceExport($request), 'commerce.xlsx');
    }

}