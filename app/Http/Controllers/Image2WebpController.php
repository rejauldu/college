<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Image2WebpController extends Controller
{
    public function convert($url, $w = 0, $h = 0, $q = 80) {
        // Get the appropriate function to create an image based on its type
        $creator = $this->imageType($url);
        if (!$creator) {
            throw new Exception("Unsupported image type.");
        }
    
        // Get original dimensions of the image
        list($width, $height) = getimagesize($url);
        $oldimage = $creator($url); // Create an image resource
    
        // Set target width and height if not provided
        if (!$w) $w = $width;
        if (!$h) $h = $height;
    
        // Create a blank image with the target dimensions
        $newimage = imagecreatetruecolor($w, $h);
    
        // Set white background for transparency in case the original image has transparency
        $white = imagecolorallocate($newimage, 255, 255, 255);
        imagefill($newimage, 0, 0, $white);
    
        // Resample the old image into the new one (resize)
        imagecopyresampled($newimage, $oldimage, 0, 0, 0, 0, $w, $h, $width, $height);
    
        // Set the new file URL and convert the image to WebP format
        $newUrl = $this->rename($url);
        imagewebp($newimage, $newUrl, $q);
    
        // Free memory
        imagedestroy($newimage);
        imagedestroy($oldimage);
    
        // Optionally delete the original file if it’s not already WebP
        if ($creator !== 'imagecreatefromwebp') {
            \File::delete($url);
        }
    
        return $newUrl;
    }
    
    /**
     * Helper function to determine the image type and return the appropriate image creation function
     */
    private function imageType($url) {
        $mime = mime_content_type($url);
        switch ($mime) {
            case 'image/jpeg':
                return 'imagecreatefromjpeg';
            case 'image/png':
                return 'imagecreatefrompng';
            case 'image/gif':
                return 'imagecreatefromgif';
            case 'image/webp':
                return 'imagecreatefromwebp';
            default:
                return null;
        }
    }
    
    /**
     * Helper function to rename the file to have a .webp extension
     */
    private function rename($url) {
        return preg_replace('/\.(jpg|jpeg|png|gif)$/i', '.webp', $url);
    }
    public function compress($url, $w=0, $h=0, $q=8) {
        $creator = $this->imageType($url);
        list($width, $height) = getimagesize($url);
        $oldimage = $creator($url);
        if(!$w)
            $w = imagesx($oldimage);
        if(!$h)
            $h = imagesy($oldimage);
        $newimage = imagecreatetruecolor($w, $h);
        $white = imagecolorallocate($newimage, 255, 255, 255);
        imagefill($newimage, 0, 0, $white);
        imagecopyresampled($newimage, $oldimage, 0, 0, 0, 0, $w, $h, $width, $height);
        header('Content-Type: image/webp');
        imagewebp($newimage, $url, $q);
        imagedestroy($newimage);
        imagedestroy($oldimage);
        return $url;
    }
}
