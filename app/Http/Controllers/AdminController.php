<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Attendance;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {
        $data = [];
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        $data['first_year'] = User::where('hsc', $batches->batch2)->where('status', 1)->count();
        $data['second_year'] = User::where('hsc', $batches->batch1)->where('status', 1)->count();
        
        // First, get the last 8 distinct dates from the attendances table
        $dates = Attendance::select(DB::raw('DATE(created_at) as date'))
            ->whereIn('hsc', [$batches->batch1, $batches->batch2]) // Filter for the batches
            ->groupBy(DB::raw('DATE(created_at)')) // Group by date
            ->orderBy(DB::raw('DATE(created_at)'), 'desc') // Order by latest date
            ->take(10) // Limit to the last 8 days
            ->pluck('date')
            ->reverse(); // Get only the dates

        // Fetch attendance data for the last 8 days for both batches
        $attendances = Attendance::select(
                DB::raw('DATE(created_at) as date, hsc, COUNT(created_at) as day')
            )
            ->whereIn(DB::raw('DATE(created_at)'), $dates) // Filter for the last 8 distinct dates
            ->whereIn('hsc', [$batches->batch1, $batches->batch2]) // Filter for both batches
            ->groupBy(DB::raw('DATE(created_at), hsc')) // Group by date and batch
            ->orderBy(DB::raw('DATE(created_at)'), 'desc') // Order by date
            ->get(); // Get the results as a collection

        // Ensure absent batches on certain days are handled with day = 0
        $batch1 = $batches->batch1;
        $batch2 = $batches->batch2;
        
        // Prepare the result collection
        $data['dates'] = [];
        $data['first_days'] = [];
        $data['second_days'] = [];
        
        foreach ($dates as $date) {
            // Check if batch1 data exists for this date
            $batch1Attendance = $attendances->first(function ($attendance) use ($date, $batch1) {
                return $attendance->date == $date && $attendance->hsc == $batch1;
            });
        
            // Check if batch2 data exists for this date
            $batch2Attendance = $attendances->first(function ($attendance) use ($date, $batch2) {
                return $attendance->date == $date && $attendance->hsc == $batch2;
            });
        
            $data['dates'][] = $date->format("j M");
            $data['second_days'][] = $batch1Attendance ? $batch1Attendance->day : 0;
            $data['first_days'][] = $batch2Attendance ? $batch2Attendance->day : 0;
        }
        return view('admin.index', $data);
    }
}