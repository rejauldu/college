<?php

namespace App\Http\Controllers;

use App\Models\Date;
use App\Models\Post;
use App\Models\PostMeta;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function home(Request $request) {
        $data = [];
        if (Cache::has('home')) {
            $data = Cache::get('home');
        } else {
            $data['slides'] = Post::where('type', 'home-slider')->latest()->get();
            $posts = PostMeta::where('meta_key', 'is_moving')->where('meta_value', 1)->pluck('post_id')->toArray();
            $data['movings'] = Post::whereIn('id', $posts)->latest()->get();
            $data['principal'] = User::where('role', config('auth.role')['principal'])->with('userMeta')->where('status', 1)->first();
            $data['vice'] = User::where('role', config('auth.role')['vice'])->with('userMeta')->where('status', 1)->first();
            Cache::put('home', $data);
        }
        if (Cache::has('notices'.$request->page)) {
            $data['notices'] = Cache::get('notices'.$request->page);
        } else {
            $data['notices'] = Post::where('type', 'notice')->with('metas')->latest()->paginate(7);
            Cache::put('notices'.$request->page, $data['notices']);
        }
        //hsc admission
        if (Cache::has('hsc_admission'.$request->page)) {
            $data['hsc_admission'] = Cache::get('hsc_admission'.$request->page);
        } else {
            $data['hsc_admission'] = Post::where('type', 'notice')->whereHas('metas', function($q){
                $q->where('meta_key', 'notice_cat')
                ->where('meta_value', 'hsc-admission');
            })->latest()->paginate(4);
            Cache::put('hsc_admission'.$request->page, $data['hsc_admission']);
        }
        //hsc routine
        if (Cache::has('hsc_routine'.$request->page)) {
            $data['hsc_routine'] = Cache::get('hsc_routine'.$request->page);
        } else {
            $data['hsc_routine'] = Post::where('type', 'notice')->whereHas('metas', function($q){
                $q->where('meta_key', 'notice_cat')
                ->Where('meta_value', 'hsc-routine');
            })->latest()->paginate(4);
            Cache::put('hsc_routine'.$request->page, $data['hsc_routine']);
        }
        
        //Degree admission
        if (Cache::has('degree_admission'.$request->page)) {
            $data['degree_admission'] = Cache::get('degree_admission'.$request->page);
        } else {
            $data['degree_admission'] = Post::where('type', 'notice')->whereHas('metas', function($q){
                $q->where('meta_key', 'notice_cat')
                ->where('meta_value', 'degree-admission');
            })->latest()->paginate(4);
            Cache::put('degree_admission'.$request->page, $data['degree_admission']);
        }
        //Degree routine
        if (Cache::has('degree_routine'.$request->page)) {
            $data['degree_routine'] = Cache::get('degree_routine'.$request->page);
        } else {
            $data['degree_routine'] = Post::where('type', 'notice')->whereHas('metas', function($q){
                $q->where('meta_key', 'notice_cat')
                ->Where('meta_value', 'degree-routine');
            })->latest()->paginate(4);
            Cache::put('degree_routine'.$request->page, $data['degree_routine']);
        }
        
        return view('home', $data);
    }

    public function principal() {
        if (Cache::has('principal')) {
            $post = Cache::get('principal');
        } else {
            $post = Post::where('type', 'principals-message')->first();
            Cache::put('principal', $post);
        }
        return view('principal', compact('post'));
    }
    public function vice() {
        $data = [];
        if (Cache::has('vice-principals-message')) {
            $data['post'] = Cache::get('vice-principals-message');
        } else {
            $data['post'] = Post::where('type', 'vice-principals-message')->first();
            Cache::put('vice-principals-message', $data['post']);
        }
        if (Cache::has('vice')) {
            $data['user'] = Cache::get('vice');
        } else {
            $data['user'] = User::where('role', config('auth.role')['vice'])->with('userMeta')->where('status', 1)->first();
            Cache::put('vice', $data['user']);
        }
        return view('vice', $data);
    }
    public function about() {
        if (Cache::has('about')) {
            $post = Cache::get('about');
        } else {
            $post = Post::where('type', 'about')->first();
            Cache::put('about', $post);
        }
        return view('about', compact('post'));
    }
    public function contact() {
        return view('contact');
    }
    public function academicRules() {
        if (Cache::has('academic-rules')) {
            $post = Cache::get('academic-rules');
        } else {
            $post = Post::where('type', 'academic-rules')->first();
            Cache::put('academic-rules', $post);
        }
        return view('academic-rules', compact('post'));
    }
    public function wall(Request $request) {
        $posts = Post::where('type', 'status')->latest()->with('user', 'comments.user', 'likes.user', 'dislikes.user')->paginate(10);
        return view('wall', compact('posts'));
    }
    public function c() {
        return view('tools.c');
    }
    private function getNoOfDays() {
        $year = date('Y');
        $month = date('n');
        
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        return [
            "days" => $daysInMonth,
            "firstDayOfWeek" => Carbon::createFromDate($year, $month, 1)->dayOfWeek
        ];
    }
    
    public function privacyPolicy() {
        return view('privacy-policy');
    }
}
