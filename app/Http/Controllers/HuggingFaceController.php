<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HuggingFaceController extends Controller
{
    public function chat(Request $request)
    {
        // Use your Pro subscription token here
        $apiKey = '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';
        $apiUrl = 'https://api-inference.huggingface.co/models/rejauldu/mistralai-bengali';

        // Prepare the input string using heredoc syntax
        $inputs = "<s>[INST] Today is 20th may 2024? [/INST]\nOk.</s>\n[INST] Tell me tomorrow's date. [/INST]";

        // Prepare the payload
        $payload = [
            'inputs' => $inputs,
            'parameters' => [
                'max_new_tokens' => 100,
                'return_full_text' => false, // Exclude prompt from generated text
            ],
        ];

        // Make the HTTP request
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $apiKey,
            'Content-Type' => 'application/json',
        ])->post($apiUrl, $payload);

        // Check if the request was successful
        if ($response->successful()) {
            // Extract the generated text from the response
            $data = $response->json()[0];
            $generatedText = $data['generated_text'];

            // Return the generated text
            return response()->json(['generated_text' => $generatedText]);
        } else {
            // Return error response if the request fails
            return response()->json([
                'error' => 'Failed to generate text',
                'status' => $response->status(),
                'response' => $response->json()
            ], $response->status());
        }
    }
}