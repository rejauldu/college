<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Attendance;

class SyncController extends Controller
{
    // URL of the remote API
    protected $remoteApiUrl = 'http://college/api/attendances/sync';

    // The password required by the remote API for authentication
    protected $apiPassword = 'hasanpursncollege';

    public function sync()
    {
        // Get all unsynced attendance records
        $attendances = Attendance::where('synced', 0)->get();

        // Prepare the data to be sent
        $data = $attendances->map(function ($attendance) {
            return [
                'roll' => $attendance->roll,
                'hsc' => $attendance->hsc,
                'created_at' => $attendance->created_at
            ];
        })->toArray();

        // Send the data in a single request
        $response = Http::post($this->remoteApiUrl, [
            'attendances' => $data,
            'password' => $this->apiPassword
        ]);

        // Check if the request was successful
        if ($response->successful()) {
            // Update the synced status for all attendances
            Attendance::where('synced', 0)->update(['synced' => 1]);

            return redirect()->back()->with('message', 'Synchronization is complete!');
        } else {
            // Handle the error case
            return redirect()->back()->with('error', 'Synchronization failed. Please try again.');
        }
    }
}