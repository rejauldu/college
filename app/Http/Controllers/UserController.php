<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Imports\UserImport;
use App\Models\Post;
use App\Models\Subject;
use App\Models\User;
use App\Models\Result;
use App\Models\UserMeta;
use DB;
use Excel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Rq;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Pagination\LengthAwarePaginator;

class UserController extends Controller
{
    protected $converter;
    public function __construct() {
        $this->middleware('admin', ['except' => [
            'teachers',
            'staffs',
            'students',
            'show',
            'update'
        ]]);
        $this->middleware('auth', ['only' => [
            'update'
        ]]);
        $this->converter = new Image2WebpController();
    }
    /**
     * Display a listing of the resource for teacher in webite.
     *
     * @return \Illuminate\Http\Response
     */
    public function teachers() {
        $users = User::where('role', '>=', 4)->where('role', '<=', 6)->where('status', 1)->with('subject', 'userMeta')->orderBy('role', 'desc')->orderBy(DB::raw('ISNULL(serial), serial'))->get();
        return view('users.teachers', compact('users'));
    }
    /**
     * Display a listing of the resource in webite.
     *
     * @return \Illuminate\Http\Response
     */
    public function staffs() {
        $users = User::where('role', 3)->where('status', 1)->with('userMeta')->latest()->paginate(100);
        return view('users.staffs', compact('users'));
    }
    /**
     * Display a listing of the resource in webite.
     *
     * @return \Illuminate\Http\Response
     */
    public function students(Request $request) {
        $result = Result::latest()->first();
        $user = auth()->user();
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        if($user && $user->role == 2)
            $users = User::with('subject', 'userMeta')->where('hsc', $user->hsc);
        elseif($request->search)
            $users = User::with('subject', 'userMeta')->wherein('hsc', [$batches->batch1, $batches->batch2]);
        else
            $users = User::with('subject', 'userMeta')->where('hsc', $result->hsc);
        $users = $users->get();
        $users = $users->sortByDesc(function($user) {
            return $user->result;
        });
        // Add serial number to each user
        $users = $users->values()->map(function($user, $index) {
            $user->serial = $index + 1; // Serial numbers starting from 1
            return $user;
        });
        $search = false;
        if($request->search) {
            $search = b2e($request->search);
            // Filter users whose name contains 'John'
            $users = $users->filter(function ($user) use($search) {
                return str_contains($user->name, $search) || $user->roll == $search;
            });
            $search = true;
        }
        $users = $this->paginate($users);
        return view('users.students', compact('users', 'search'));
    }

    /**
     * Display a single listing.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::where('id', $id)->first();
        $posts = Post::where('type', 'status')->where('user_id', $user->id)->latest()->with('user', 'comments.user', 'likes.user', 'dislikes.user')->paginate(10);
        return view('users.show', compact('user', 'posts'));
    }

    /**
     * Display a listing of the resource in dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request) {
        $data = [];
        $users = User::latest();
        if($request->phone) {
            $users = $users->where('phone', 'like', '%'.$request->phone.'%');
            $data['phone'] = $request->phone;
        }
        if($request->roll) {
            $users = $users->where('roll', $request->roll);
            $data['roll'] = $request->roll;
        }
        if($request->hsc) {
            $users = $users->where('hsc', $request->hsc);
            $data['hsc'] = $request->hsc;
        }

        $data['users'] = $users->paginate(100);
        return view('users.list', $data);
    }
    /**
     * Display the user's profile form.
     */
    public function edit(User $user): View
    {
        $subjects = Subject::all();
        $auth = auth()->user();
        return view('users.edit', compact('subjects', 'user', 'auth'));
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request, $id): RedirectResponse
    {
        $auth = auth()->user();
        if(!($auth->id == $id || $auth->role>1)) {
            return redirect()->back()->with('status', 'profile-updated');
        }
        $data = $request->except('_token', '_method');
        $user = User::find($id);
        $file = $request->file('photo');
        if ($file) {
            $destination_path = 'images/profile';
            $only_name = time() . '-image'; 
            $new_name = $only_name . '.' . $file->getClientOriginalExtension();
            $file->move($destination_path, $new_name);

            // Get width and height here
            list($w, $h) = getimagesize($destination_path . '/' . $new_name);

            // Now you can use w and h as needed
            $this->converter->convert($destination_path . '/' . $new_name, 210, $h/$w*210);

            $data['photo'] = $only_name . '.webp';
            $directory = "images/profile/";
            File::delete($directory . $user->file);
            $this->photoUpdate($data['photo']);
        }
        if(isset($data['bcs'])) {
            $data['serial'] = $data['bcs'];
        }
        $user->update($data);

        return redirect()->back()->with('status', 'profile-updated');
    }

    public function updateStatus(Request $request) {
        $user = User::find($request->id)->update(['status' => $request->status]);
    }

    /**
     * Update meta info.
     */
    public function meta(Request $request, $id): RedirectResponse
    {
        UserMeta::updateOrCreate(
        [
            'user_id'   => $id,
            'meta_key'  => 'designation'
        ],
        [
            'meta_value' => $request->designation
        ]);
        return redirect()->back()->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
    /**
     * Display the import form.
     */
    public function import(): View
    {
        $batches = appMeta([
            'batch1',
            'batch2'
        ]);
        return view('users.import', compact('batches'));
    }

    /**
     * Upload users
     */
    public function upload(Request $request)
    {
        set_time_limit(600);
        
        $extensions = array("xls","xlsx","csv");

        $result = array($request->file('users')->getClientOriginalExtension());

        if(in_array($result[0],$extensions)){
            Excel::import(new UserImport, $request->file('users'), 12);
        }else{
            request()->validate([
                'users' => 'required|mimes:csv, xls, xlsx|max:2048'
            ]);
        }
        
        return redirect()->back()->with('message', 'User Imported');
    }
    /*
    * Paginator
    */
    public function paginate($users) {

        // Get current page from URL (default to 1 if not set)
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Define how many items you want per page
        $perPage = 100;

        // Slice the collection to get the items to display in current page
        $currentPageItems = $users->slice(($currentPage - 1) * $perPage, $perPage)->values();

        // Create the paginator instance
        $paginatedUsers = new LengthAwarePaginator($currentPageItems, $users->count(), $perPage);

        // Append query parameters to paginator links (optional)
        $paginatedUsers->setPath(Rq::url());

        // Return the paginated users
        return $paginatedUsers;

    }
    public function photoUpdate($photo) {
        $directory = "images/profile/";
        $destination = "posts/";
        $user = auth()->user();
        $data['user_id'] = $user->id;
        $data['link'] = $photo;
        $data['body'] = "Updated profile photo";
        $data['type'] = "status";
        Post::create($data);
        copy($directory.$photo, $destination.$photo);
    }
    
}