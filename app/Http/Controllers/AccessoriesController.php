<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Artisan;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class AccessoriesController extends Controller
{
    public function sitemap() {
        $routine = Post::where('type', 'routine')->first();
        $about = Post::where('type', 'about')->first();
        $principal = Post::where('type', 'principals-message')->first();
        $vice = Post::where('type', 'vice-principals-message')->first();
        $sitemap = Sitemap::create()
            ->add(Url::create("/")->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)->setPriority(0.7))
            ->add(Url::create('routine')->setLastModificationDate($routine->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)->setPriority(0.4))
            ->add(Url::create('result')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.4))
            ->add(Url::create('about')->setLastModificationDate($about->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.9))
            ->add(Url::create('contact')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.1))
            ->add(Url::create('academic-rules')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)->setPriority(0.01))
            ->add(Url::create('login')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)->setPriority(0.01))
            ->add(Url::create('teachers')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.8))
            ->add(Url::create('staffs')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.8))
            ->add(Url::create('principals-message')->setLastModificationDate($principal->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.3))
            ->add(Url::create('vice-principals-message')->setLastModificationDate($vice->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.3));

        $notices = Post::where('type', 'notice')->get();
        foreach ($notices as $notice) {
            $sitemap = $sitemap->add(Url::create('notices/'.$notice->id . '/' . urlencode($notice->title))->setLastModificationDate($notice->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.5));
        }
        
        $blog = Post::where('type', 'blog')->get();
        $sitemap = $sitemap->add(Url::create('blog')->setLastModificationDate(Carbon::yesterday())->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)->setPriority(0.5));
        foreach ($blog as $post) {
            $sitemap = $sitemap->add(Url::create('blog/'.$post->id . '/' . urlencode($post->title))->setLastModificationDate($post->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)->setPriority(0.5));
        }
        
        $users = User::all();
        foreach ($users as $user) {
            $sitemap = $sitemap->add(Url::create('users/'.$user->id . '/' . urlencode($user->name))->setLastModificationDate($user->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)->setPriority(0.5));
            if($user->hsc>2024) {
                $sitemap = $sitemap->add(Url::create('attendances/' . $user->hsc . '/' . $user->roll . '/' . urlencode($user->name))->setLastModificationDate($user->updated_at)->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)->setPriority(0.4));
            }
        }
        // $sitemap->writeToFile("sitemap.xml");
        return $sitemap;
    }
    public function cache() {
        Artisan::call('view:cache');
        Artisan::call('config:cache');
        Artisan::call('route:cache');
        return redirect()->back()->with('message', 'Cache stored');
    }
    public function clearCache() {
        Cache::flush();
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('optimize:clear');
        return redirect()->back()->with('message', 'Cache cleared');
    }
    public static function bladeDirectivesForAuthorization()
    {
		Blade::directive('admin', function ($name=null) {
			return '<?php if($auth && $auth->role == config("auth.role")["admin"]): ?>';
		});
		Blade::directive('teacher', function () {
			return '<?php if ($auth && $auth->role >= config("auth.role")["teacher"]): ?>';
		});
        Blade::directive('staff', function () {
            return '<?php if ($auth && $auth->role == config("auth.role")["staff"] || $auth->role == config("auth.role")["admin"]): ?>';
        });
        Blade::directive('student', function () {
            return '<?php if ($auth && $auth->role == config("auth.role")["student"] || $auth->role == config("auth.role")["admin"]): ?>';
        });
        Blade::directive('user', function () {
            return '<?php if ($auth && $auth->role == config("auth.role")["user"] || $auth->role == config("auth.role")["admin"]): ?>';
        });
    }
}