<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id',
        'react',
    ];
    
    protected $primaryKey = null;
    
    public $incrementing = false;

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
    public function post() {
        return $this->belongsTo('App\Models\Post');
    }
}