<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meta_key',
        'meta_value',
        'post_id',
    ];

    public $timestamps = false;
}
