<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meta_key',
        'meta_value',
        'user_id',
    ];

    public $timestamps = false;
}
