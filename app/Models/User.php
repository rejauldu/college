<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\AppMeta;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Result;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Compoships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'photo',
        'roll',
        'ssc',
        'hsc',
        'bcs',
        'serial',
        'subject_id',
        'status',
        'role',
        'note',
        'birthday',
        'gender',
        'photos',
        'posts',
        'videos'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function subject() {
        return $this->belongsTo('App\Models\Subject');
    }
    public function userMeta() {
        return $this->hasMany('App\Models\UserMeta');
    }
    public function getMetaAttribute() {
        return (object)$this->userMeta->pluck('meta_value', 'meta_key')->toArray();
    }
    public function batch() {
        return $this->belongsTo('App\Models\AppMeta', 'hsc', 'meta_value');
    }
    public function getResultAttribute() {
        $result = Result::where('roll', $this->roll)
            ->where('hsc', $this->hsc)
            ->latest()
            ->first();
        $mark = $this->getMark($result);
        return $mark;
    }
    function getMark($result) {
        if($this->roll > 600) {
            return $this->getCommerce($result);
        } elseif($this->roll > 500) {
            return $this->getScience($result);
        } else {
            return $this->getArts($result);
        }
    }
    function getCommon($result) {
        if(!$result)
            return 0;
        $id = $result->exam_type_id;
        $total = $this->getTotal($id, $result->bangla_cq, $result->bangla_mcq);
        $total += $this->getTotal($id, $result->english);
        $total += $this->getTotal($id, $result->ict_cq, $result->ict_mcq, $result->ict_prac);
        $total += $this->getTotal($id, $result->agri_cq, $result->agri_mcq, $result->agri_prac);
        return $total;
    }
    function getArts($result) {
        if(!$result)
            return 0;
        $id = $result->exam_type_id;
        $total = $this->getCommon($result);
        $total += $this->getTotal($id, $result->social_cq, $result->social_mcq);
        $total += $this->getTotal($id, $result->political_cq, $result->political_mcq);
        $total += $this->getTotal($id, $result->islamic_cq, $result->islamic_mcq);
        $total += $this->getTotal($id, $result->logic_cq, $result->logic_mcq);
        $total += $this->getTotal($id, $result->geography_cq, $result->geography_mcq);
        $total += $this->getTotal($id, $result->philosophy_cq, $result->philosophy_mcq);
        $total += $this->getTotal($id, $result->economics_cq, $result->economics_mcq);
        return $total;
    }
    function getScience($result) {
        if(!$result)
            return 0;
        $id = $result->exam_type_id;
        $total = $this->getCommon($result);
        $total += $this->getTotal($id, $result->physics_cq, $result->physics_mcq, $result->physics_prac);
        $total += $this->getTotal($id, $result->chemistry_cq, $result->chemistry_mcq, $result->chemistry_prac);
        $total += $this->getTotal($id, $result->math_cq, $result->math_mcq, $result->math_prac);
        $total += $this->getTotal($id, $result->biology_cq, $result->biology_mcq, $result->biology_prac);
        return $total;
    }
    function getCommerce($result) {
        if(!$result)
            return 0;
        $id = $result->exam_type_id;
        $total = $this->getCommon($result);
        $total += $this->getTotal($id, $result->accounting_cq, $result->accounting_mcq);
        $total += $this->getTotal($id, $result->management_cq, $result->management_mcq);
        $total += $this->getTotal($id, $result->finance_cq, $result->finance_mcq);
        $total += $this->getTotal($id, $result->economics_cq, $result->economics_mcq);
        return $total;
    }
    function getTotal($id, $cq=0, $mcq=null, $prac=null) {
        $cq = $cq<0?0:$cq;
        $mcq = $mcq<0?0:$mcq;
        $prac = $prac<0?0:$prac;

        if($id<10 && $prac !== null) {
            if($cq>=17 && $mcq >= 8 && $prac >= 8)
                return $cq+$mcq+$prac;
            else
                return 0;
        }
        if($id<10) {
            if($mcq !== null) {
                if($cq>=23 && $mcq >= 10)
                    return $cq+$mcq;
                else
                    return 0;
            }
            if($cq>=33)
                return $cq;
            else
                return 0;
        } else {
            if($mcq !== null) {
                if($cq>=10 && $mcq >= 7)
                    return $cq+$mcq;
                else
                    return 0;
            }
            if($cq>=17)
                return $cq;
            else
                return 0;
        }
    }
}
