<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public $timestamps = false;
}