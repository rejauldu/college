<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hsc',
    ];

    public $timestamps = false;
}
