<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'link',
        'category_id',
        'type',
        'user_id',
        'updated_at',
        'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'updated_at' => 'datetime',
		'created_at' => 'datetime',
    ];

    public function metas() {
        return $this->hasMany('App\Models\PostMeta');
    }
    public function user() {
        return $this->belongsTo('App\Models\User');
    }
    public function comments() {
        return $this->hasMany('App\Models\Comment');
    }
    public function images() {
        return $this->hasMany('App\Models\Image');
    }
    public function likes() {
        return $this->hasMany('App\Models\Like')->where('react', 1);
    }
    public function dislikes() {
        return $this->hasMany('App\Models\Like')->where('react', 2);
    }
}