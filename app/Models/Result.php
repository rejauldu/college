<?php

namespace App\Models;

use Awobaz\Compoships\Database\Eloquent\Model;

class Result extends Model {
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bangla_cq',
        'bangla_mcq',
        'english',
        'ict_cq',
        'ict_mcq',
        'ict_prac',
        'agri_cq',
        'agri_mcq',
        'agri_prac',
        'social_cq',
        'social_mcq',
        'political_cq',
        'political_mcq',
        'islamic_cq',
        'islamic_mcq',
        'logic_cq',
        'logic_mcq',
        'geography_cq',
        'geography_mcq',
        'geography_prac',
        'philosophy_cq',
        'philosophy_mcq',
        'economics_cq',
        'economics_mcq',
        'physics_cq',
        'physics_mcq',
        'physics_prac',
        'chemistry_cq',
        'chemistry_mcq',
        'chemistry_prac',
        'math_cq',
        'math_mcq',
        'math_prac',
        'biology_cq',
        'biology_mcq',
        'biology_prac',
        'accounting_cq',
        'accounting_mcq',
        'management_cq',
        'management_mcq',
        'finance_cq',
        'finance_mcq',
        'exam_type_id',
        'roll',
        'hsc',
        'updated_at',
        'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'updated_at' => 'datetime',
		'created_at' => 'datetime',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', ['roll', 'hsc'], ['roll', 'hsc']);
    }
    public function type() {
        return $this->belongsTo('App\Models\ExamType', 'exam_type_id', 'id');
    }
}