<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'post_id',
        'is_temporary',
        'created_at'
    ];

    const UPDATED_AT = null;
    
    public function post() {
        return $this->belongsTo('App\Models\Post');
    }
}
