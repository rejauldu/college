import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'theme': {
                    'primary': '#3490dc',
                    'light': '#6dbee7',
                    'secondary': '#FF7100',
                    'gray': '#888',
                    'code': '#282c34'
                }
            },
            screens: {
                'xs': '400px',
                ...defaultTheme.screens,
            },
        },
    },

    plugins: [forms],
};
